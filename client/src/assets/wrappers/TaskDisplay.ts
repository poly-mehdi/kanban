import styled from 'styled-components'

const Wrapper = styled.main`
  .task-display {
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.3);
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    visibility: hidden;
    z-index: -1;
    opacity: 0;
    transition: var(--transition);

    .content {
      width: 90vw;
      max-height: calc(100vh - var(--nav-height) - 7rem);
      background-color: var(--background-color);
      padding: 2rem 2rem;
      border-radius: 0.75rem;
      overflow-y: auto;

      @media screen and (min-width: 768px) {
        width: 60vw;
        min-height: fit-content;
      }

      @media screen and (min-width: 992px) {
        width: 594.6px;
      }

      .task-container {
        .task-title {
          display: flex;
          align-items: center;
          gap: 1rem;
          justify-content: space-between;

          h2 {
            color: var(--text-color);
            margin-bottom: 1rem;
            line-height: 2ch;
            letter-spacing: normal;
          }

          .task-title-end {
            display: flex;
            gap: 1rem;
            align-items: center;

            .title-cross {
              cursor: pointer;
            }

            .ellipsis-container {
              position: relative;

              .ellipsis {
                cursor: pointer;
              }

              .dropdown {
                position: absolute;
                visibility: hidden;
                z-index: 100;
                top: 25px;
                right: 0;
                width: 10rem;
                display: flex;
                flex-direction: column;
                border: 0.5px solid var(--grey-primary);
                border-radius: 1rem;

                @media (min-width: 768px) {
                  width: 12rem;
                }

                .btn-dropdown {
                  background-color: var(--background-color);
                  color: var(--text-color);
                  padding: 0.5rem 1rem;
                  cursor: pointer;

                  width: 100%;
                  font-size: 1rem;

                  &:hover {
                    background-color: var(--background-color-secondary);
                  }
                }

                .btn-dropdown:first-child {
                  border-bottom-left-radius: 0;
                  border-bottom-right-radius: 0;
                  color: var(--grey-primary);
                }

                .btn-dropdown:last-child {
                  border-top-left-radius: 0;
                  border-top-right-radius: 0;
                  color: var(--red-primary);
                }
              }

              .show-dropdown {
                visibility: visible;
              }
            }
          }
        }

        .description {
          color: var(--grey-primary);
          margin-bottom: 1rem;
          font-size: 0.825rem;
          font-weight: 400;
          line-height: 3ch;
        }

        .subtasks-title {
          color: var(--task-subtitle);
          margin-bottom: 1rem;
          font-size: 0.825rem;
        }

        .subtasks-container {
          list-style-type: none;
          padding: 0;
          margin: 0;
          display: flex;
          flex-direction: column;
          gap: 0.175rem;

          .subtasks {
            font-size: 1rem;
            margin-bottom: 0.5rem;
            background-color: var(--background-color-secondary);
            padding: 1rem 1rem;
            display: flex;
            align-items: center;
            gap: 1rem;
            border-radius: 0.25rem;

            .checkbox {
              cursor: pointer;
              accent-color: var(--purple-primary);
            }

            .subtask-title {
              font-weight: 600;
              font-size: 0.875rem;
            }
            .completed {
              text-decoration: line-through;
              color: var(--grey-primary);
            }
          }
        }

        .status-container {
          display: flex;
          align-items: center;
          justify-content: space-between;
          border: 0.5px solid var(--grey-primary);
          padding: 0.75rem 0.5rem;
          border-radius: 0.5rem;
          cursor: pointer;
          position: relative;

          .status-dropdown {
            position: absolute;
            top: calc(100% + 0.5rem);
            left: 0;
            right: 0;
            z-index: 200;
            background-color: var(--background-color);
            border: 0.5px solid var(--grey-primary);
            border-radius: 0.5rem;
            box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, 0.1);
            padding: 0.5rem 0;
            list-style: none;

            .status-option {
              padding: 0.5rem 1rem;
              cursor: pointer;

              &:hover {
                background-color: var(--background-color-secondary);
              }
            }
          }
        }
      }
    }
  }

  .show-task {
    visibility: visible;
    z-index: 99;
    opacity: 1;
  }
`

export default Wrapper
