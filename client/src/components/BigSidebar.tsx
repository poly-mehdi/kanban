import Wrapper from '../assets/wrappers/BigSidebar'
import logo from '../assets/logo.svg'
import {
  board as boardIcon,
  boardPurple,
  boardWhite,
  hide,
} from '../assets/images'
import Toggle from './Toggle'
import { useAppSelector, useAppDispatch } from '../hooks'
import { toggleBigSidebar } from '../features/sidebar/bigSidebarSlice'
import { setCurrentBoard } from '../features/currentBoard/currentBoardSlice'
import { openAddBoard } from '../features/dialog/addBoardSlice'

const BigSidebar = () => {
  const isSidebarOpen = useAppSelector((state) => state.bigSidebarState.isOpen)
  const allBoards = useAppSelector((state) => state.boardsState.boards)

  const currentBoard = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )

  const dispatch = useAppDispatch()

  return (
    <Wrapper>
      <div
        className={
          isSidebarOpen
            ? 'sidebar-container show-sidebar'
            : 'sidebar-container '
        }
      >
        <div className='content'>
          <div className='sidebar-top'>
            <header className='logo'>
              <img src={logo} alt='logo' />
              <p>kanban</p>
            </header>
            <div className='boards-container'>
              <p className='boards-title'>{`all boards (${allBoards.length})`}</p>
              <div className='boards'>
                {allBoards.map((board) =>
                  board._id === currentBoard ? (
                    <div
                      className='board active'
                      onClick={() => dispatch(setCurrentBoard(board._id))}
                      key={board._id}
                    >
                      <img src={boardWhite} alt='board' />
                      <p>{board.name}</p>
                    </div>
                  ) : (
                    <div
                      key={board._id}
                      className='board'
                      onClick={() => dispatch(setCurrentBoard(board._id))}
                    >
                      <img src={boardIcon} alt='board' />
                      <p>{board.name}</p>
                    </div>
                  )
                )}
              </div>
              {allBoards.length > 0 && (
                <div
                  className='boards-new'
                  onClick={() => dispatch(openAddBoard())}
                >
                  <img src={boardPurple} alt='board' />
                  <p>+Create new board</p>
                </div>
              )}
            </div>
          </div>
          <div className='sidebar-bottom'>
            <Toggle />
            <div
              className='sidebar-switch'
              onClick={() => dispatch(toggleBigSidebar())}
            >
              <img src={hide} alt='hide' />
              <p>hide sidebar </p>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}
export default BigSidebar
