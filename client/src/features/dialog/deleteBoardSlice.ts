import { createSlice } from '@reduxjs/toolkit'

type DeleteBoardState = {
  isOpen: boolean
}

const initialState: DeleteBoardState = {
  isOpen: false,
}

const deleteBoardSlice = createSlice({
  name: 'deleteBoard',
  initialState,
  reducers: {
    openDeleteBoard: (state) => {
      state.isOpen = true
    },
    closeDeleteBoard: (state) => {
      state.isOpen = false
    },
  },
})

export const { openDeleteBoard, closeDeleteBoard } = deleteBoardSlice.actions

export default deleteBoardSlice.reducer
