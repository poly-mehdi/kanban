import styled from 'styled-components'

const Wrapper = styled.main`
  .board-delete {
    position: fixed;
    left: 0;
    background-color: rgba(0, 0, 0, 0.3);
    width: 100%;
    height: 100%;
    visibility: hidden;
    z-index: -1;
    opacity: 0;
    transition: var(--transition);
    display: grid;
    place-items: center;

    .content {
      width: 90vw;
      max-height: calc(100vh - var(--nav-height) - 7rem);
      margin: 0 auto;
      background-color: var(--background-color);
      margin-top: 1rem;
      padding: 2rem 1rem;
      border-radius: 0.75rem;
      overflow-y: auto;

      @media screen and (min-width: 768px) {
        width: 60vw;
        min-height: fit-content;
      }

      @media screen and (min-width: 992px) {
        width: 594.6px;
      }

      .container {
        display: flex;
        flex-direction: column;
        gap: 0.75rem;
        width: 100%;

        .title {
          h2 {
            color: var(--red-primary);
            margin-bottom: 1rem;
            line-height: 2ch;
            letter-spacing: normal;
          }
        }

        .description {
          color: var(--grey-primary);
          line-height: 1.5;
          font-weight: 400;
          font-size: 0.75rem;
          margin-bottom: 1rem;
        }

        .btn-container {
          display: flex;
          flex-direction: column;
          gap: 0.75rem;
          text-align: center;
          font-weight: 500;

          .btn-delete {
            color: var(--white-primary);
            background-color: var(--red-primary);
          }

          .btn-cancel {
            color: var(--purple-primary);
            background-color: var(--white-tertiary);
          }
        }
      }
    }
  }

  .show-delete {
    visibility: visible;
    z-index: 99;
    opacity: 1;
  }
`

export default Wrapper
