import { FormRowProps } from '../utils/types'

const FormRow = ({
  type,
  name,
  labelText,
  value,
  autoComplete,
  placeholder,
}: FormRowProps) => {
  return (
    <div className='form-row'>
      <label htmlFor={name} className='form-label'>
        {labelText || name}
      </label>
      <input
        type={type}
        id={name}
        name={name}
        className='form-input'
        value={value}
        placeholder={placeholder || undefined}
        required
        autoComplete={autoComplete}
      />
    </div>
  )
}

export default FormRow
