import styled from 'styled-components'

const Wrapper = styled.main`
  .task-display {
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.3);
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    visibility: hidden;
    z-index: -1;
    opacity: 0;
    transition: var(--transition);

    .content {
      width: 90vw;
      max-height: calc(100vh - var(--nav-height) - 7rem);
      margin: 0 auto;
      background-color: var(--background-color);
      margin-top: 1rem;
      padding: 2rem 1rem;
      border-radius: 0.75rem;
      overflow-y: auto;

      @media screen and (min-width: 768px) {
        width: 60vw;
        min-height: fit-content;
      }

      @media screen and (min-width: 992px) {
        width: 594.6px;
      }

      .task-container {
        .task-title {
          display: flex;
          align-items: center;
          gap: 1rem;
          justify-content: space-between;

          h2 {
            color: var(--text-color);
            margin-bottom: 1rem;
            line-height: 2ch;
            letter-spacing: normal;
          }

          .title-cross {
            color: red;
            cursor: pointer;
          }
        }

        label {
          color: var(--task-subtitle);
          margin-bottom: 1rem;
          font-size: 0.825rem;
          text-transform: capitalize;
          letter-spacing: normal;
          font-weight: 600;
        }

        input {
          &:focus {
            border: 1px solid var(--red-primary);
          }
        }

        .form-row {
          .form-input {
            border: 1px solid var(--white-tertiary);
            border-radius: 0.25rem;
            font-size: 0.825rem;
            padding: 0.75rem 0.5rem;

            &::placeholder {
              color: var(--grey-primary);
            }

            &:focus {
              border: 2px solid var(--purple-primary);
              outline: none;
            }
          }

          textarea {
            width: 100%;
            border: 1px solid var(--white-tertiary);
            border-radius: 0.25rem;
            padding: 0.5rem;
            resize: none;
            font-size: 0.825rem;
            font-family: var(--font-family);
            background-color: var(--background-color);
            color: var(--text-color);

            &::placeholder {
              color: var(--grey-primary);
            }

            &:focus {
              outline: none;
              border: 2px solid var (--purple-primary);
            }
          }
        }

        .subtasks-title {
          margin-bottom: 1rem;
        }

        .subtasks-container {
          display: flex;
          flex-direction: column;
          gap: 0.5rem;
          margin: 1rem 0;

          .subtask-container {
            display: flex;
            align-items: center;
            gap: 1rem;

            .subtask {
              padding: 0.75rem 0.5rem;
              border-radius: 0.5rem;
              border: 1px solid var(--white-tertiary);
              width: 100%;
              background-color: var(--background-color);
              color: var(--text-color);

              &::placeholder {
                color: var(--grey-primary);
              }

              &:focus {
                outline: none;
                border: 2px solid var(--purple-primary);
              }
            }

            .cross {
              color: var(--grey-primary);
              cursor: pointer;

              &:hover {
                color: var(--red-primary);
              }
            }
          }

          .btn-add {
            background-color: var(--white-tertiary);
            color: var(--purple-primary);
            font-weight: 500;
            letter-spacing: normal;
            font-size: 0.825rem;
          }
        }

        .status-container {
          display: flex;
          align-items: center;
          justify-content: space-between;
          border: 0.5px solid var(--grey-primary);
          padding: 0.75rem 0.5rem;
          border-radius: 0.5rem;
          margin-top: 1rem;
          position: relative;
          cursor: pointer;

          .chevron {
            margin-left: 1rem;
          }

          .dropdown {
            position: absolute;
            top: 100%;
            left: 0;
            right: 0;
            background: var(--background-color);
            border: 1px solid var(--grey-primary);
            border-radius: 0.25rem;
            margin-top: 0.5rem;
            z-index: 20000;

            .dropdown-item {
              padding: 0.75rem 1rem;
              cursor: pointer;

              &:hover {
                background-color: var(--background-color-secondary);
              }
            }
          }
        }

        .btn-create {
          background-color: var(--purple-primary);
          color: var(--white-primary);
          font-weight: 500;
          letter-spacing: normal;
          font-size: 0.825rem;
          padding: 0.75rem 0.5rem;
          width: 100%;
          margin-top: 1rem;
          cursor: pointer;
        }
      }
    }
  }

  .show-task {
    visibility: visible;
    z-index: 99;
    opacity: 1;
  }
`

export default Wrapper
