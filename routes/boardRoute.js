import { Router } from 'express'
const router = Router()

import {
  getAllBoards,
  createBoard,
  getBoard,
  updateBoard,
  deleteBoard,
} from '../controllers/boardController.js'

import {
  validateBoardInput,
  validateIdParam,
} from '../middleware/validationMiddleware.js'

import { checkForTestUser } from '../middleware/authMiddleware.js'

router
  .route('/')
  .get(getAllBoards)
  .post(checkForTestUser, validateBoardInput, createBoard)
router
  .route('/:id')
  .get(validateIdParam, getBoard)
  .patch(checkForTestUser, validateIdParam, updateBoard)
  .delete(checkForTestUser, validateIdParam, deleteBoard)

export default router
