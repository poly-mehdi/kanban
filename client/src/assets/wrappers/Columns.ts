import styled from 'styled-components'

const Wrapper = styled.main`
  .empty {
    height: calc(100vh - var(--nav-height));
    display: grid;
    place-items: center;

    .columns-container-empty {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      gap: 1rem;

      .text-empty {
        font-size: 1.25rem;
        font-weight: 600;
        color: var(--grey-primary);
        text-align: center;
      }

      .btn-empty {
        background-color: var(--purple-primary);
        cursor: pointer;
        height: 2.5rem;
        width: 10rem;
        border-radius: 1.5rem;
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 0.25rem;

        .plus {
          font-weight: 600;
          color: var(--white-primary);
          font-size: 1rem;
          padding-bottom: 3px;
        }

        .plus-text {
          text-transform: capitalize;
          font-size: 0.825rem;
          font-weight: 600;
          color: var(--white-primary);
        }
      }
    }
  }

  .columns-container {
    padding: 2rem;
    height: calc(100vh - var(--nav-height));
    overflow-x: auto;
    display: flex;
    flex-wrap: nowrap;
    gap: 2rem;
  }

  .columns {
    display: flex;
    gap: 2rem;
  }

  .column {
    min-width: 20rem;
    @media (min-width: 768px) {
      min-width: 25rem;
    }
  }
`

export default Wrapper
