import styled from 'styled-components'

const Wrapper = styled.div`
  background-color: var(--add-column);
  height: calc(100vh - var(--nav-height) - 2.5rem - 5rem);
  margin-top: 3rem;
  width: 20rem;
  display: grid;
  place-items: center;
  cursor: pointer;
  padding-right: 2rem;
  border-radius: 0.5rem;

  .content {
    h1 {
      color: var(--grey-primary);
    }
  }
`

export default Wrapper
