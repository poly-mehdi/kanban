import { createSlice } from '@reduxjs/toolkit'

type EditBoardState = {
  isOpen: boolean
}

const initialState: EditBoardState = {
  isOpen: false,
}

const editBoardSlice = createSlice({
  name: 'editBoard',
  initialState,
  reducers: {
    openEditBoard: (state) => {
      state.isOpen = true
    },
    closeEditBoard: (state) => {
      state.isOpen = false
    },
  },
})

export const { openEditBoard, closeEditBoard } = editBoardSlice.actions

export default editBoardSlice.reducer
