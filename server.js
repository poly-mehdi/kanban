import * as dotenv from 'dotenv'
import 'express-async-errors'
dotenv.config()
import express from 'express'
const app = express()
import morgan from 'morgan'
import mongoose, { mongo } from 'mongoose'

//routers
import boardRouter from './routes/boardRoute.js'
import columnRouter from './routes/columnRoute.js'
import taskRouter from './routes/taskRouter.js'
import authRouter from './routes/authRouter.js'
import userRouter from './routes/userRouter.js'

//public
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'

//middlewares
import errorHandlerMiddleware from './middleware/errorHandlerMiddleware.js'
import { authenticateUser } from './middleware/authMiddleware.js'
import cookieParser from 'cookie-parser'

const __dirname = dirname(fileURLToPath(import.meta.url))
app.use(express.static(path.join(__dirname, './client/dist')))

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

app.use(cookieParser())
app.use(express.json())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/test', (req, res) => {
  res.send('Test route')
})

app.use('/api/v1/boards', authenticateUser, boardRouter)
app.use('/api/v1/columns', authenticateUser, columnRouter)
app.use('/api/v1/tasks', authenticateUser, taskRouter)
app.use('/api/v1/users', authenticateUser, userRouter)
app.use('/api/v1/auth', authRouter)

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, './client/dist', 'index.html'))
})

app.use('*', (req, res) => {
  res.status(404).json({ error: 'route not found' })
})

app.use(errorHandlerMiddleware)

const port = process.env.PORT || 5100

try {
  await mongoose.connect(process.env.MONGO_URI)
  app.listen(port, () => {
    console.log(`Server is running on port ${port}...`)
  })
} catch (error) {
  console.log(error)
  process.exit(1)
}
