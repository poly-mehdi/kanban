import mongoose from 'mongoose'
import Board from '../models/boardModel.js'
import Column from '../models/columnModel.js'
import Task from '../models/taskModel.js'
import { StatusCodes } from 'http-status-codes'

export const getAllBoards = async (req, res) => {
  const boards = await Board.find({ createdBy: req.user.userId })
  res.status(StatusCodes.OK).json({ boards })
}

export const createBoard = async (req, res) => {
  const { name, columns } = req.body

  try {
    const board = await Board.create({ name, createdBy: req.user.userId })

    const createdColumns = await Promise.all(
      columns.map((column) =>
        Column.create({
          name: column.name,
          boardId: board._id,
          createdBy: req.user.userId,
        })
      )
    )

    const columnIds = createdColumns.map((col) => col._id)
    const updatedBoard = await Board.findByIdAndUpdate(
      board._id,
      { columns: columnIds },
      { new: true }
    )
    res.status(StatusCodes.CREATED).json({ updatedBoard, createdColumns })
  } catch (error) {
    res
      .status(StatusCodes.BAD_REQUEST)
      .json({ msg: 'Error creating board', error })
  }
}

export const getBoard = async (req, res) => {
  const board = await Board.findById(req.params.id)

  res.status(StatusCodes.OK).json({ board })
}

export const updateBoard = async (req, res) => {
  const session = await mongoose.startSession()
  session.startTransaction()
  try {
    const { name, columns } = req.body
    const boardId = req.params.id
    const board = await Board.findById(boardId).session(session)

    if (!board) {
      throw new NotFoundError('Board not found')
    }

    const existingColumns = await Column.find({ boardId }).session(session)
    const existingColumnIds = existingColumns.map((column) =>
      column._id.toString()
    )
    const newColumnIds = columns.map((column) => column._id).filter((id) => id) // Exclude newly added columns without _id

    // Find columns to delete
    const columnsToDelete = existingColumns.filter(
      (column) => !newColumnIds.includes(column._id.toString())
    )

    // Delete tasks in columns to be deleted
    for (const column of columnsToDelete) {
      await Task.deleteMany({ columnId: column._id }).session(session)
    }

    // Delete columns
    await Column.deleteMany({
      _id: { $in: columnsToDelete.map((column) => column._id) },
    }).session(session)

    // Update board name
    board.name = name
    await board.save({ session })

    // Update or create columns
    const updatedColumns = []
    for (const column of columns) {
      if (column._id) {
        const updatedColumn = await Column.findByIdAndUpdate(
          column._id,
          { name: column.name },
          { new: true, session }
        )
        updatedColumns.push(updatedColumn)
      } else {
        const newColumn = await Column.create(
          [
            {
              name: column.name,
              boardId: board._id,
              createdBy: req.user.userId,
            },
          ],
          { session }
        )
        updatedColumns.push(newColumn[0])
      }
    }

    // Update board columns
    board.columns = updatedColumns.map((column) => column._id)
    await board.save({ session })

    await session.commitTransaction()
    session.endSession()

    res.status(StatusCodes.OK).json({
      msg: 'board modified',
      updatedBoard: board,
      updatedColumns: updatedColumns.filter(
        (column) => !existingColumnIds.includes(column._id.toString())
      ),
      removedColumns: columnsToDelete.map((column) => column._id),
    })
  } catch (error) {
    await session.abortTransaction()
    session.endSession()
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ msg: 'Error updating board', error })
  }
}

export const deleteBoard = async (req, res) => {
  const session = await mongoose.startSession()
  session.startTransaction()
  try {
    const board = await Board.findById(req.params.id).session(session)
    if (!board) {
      throw new NotFoundError('Board not found')
    }

    const columns = await Column.find({ boardId: board._id }).session(session)
    for (const column of columns) {
      await Task.deleteMany({ columnId: column._id }).session(session)
    }
    await Column.deleteMany({ boardId: board._id }).session(session)
    await Board.findByIdAndDelete(req.params.id).session(session)

    await session.commitTransaction()
    session.endSession()

    res.status(StatusCodes.OK).json({ msg: 'board and its contents deleted' })
  } catch (error) {
    await session.abortTransaction()
    session.endSession()
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ msg: 'Error deleting board', error })
  }
}
