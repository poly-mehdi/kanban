import Wrapper from '../assets/wrappers/LandingPage'
import logo from '../assets/logo.svg'
import { Link } from 'react-router-dom'

const Landing = () => {
  return (
    <Wrapper>
      <nav className='logo'>
        <img src={logo} alt='logo' />
        <p>kanban</p>
      </nav>
      <div className='container landing'>
        <div className='info'>
          <h1 className='title'>
            Project <span>Management</span> Tool
          </h1>
          <h3 className='description'>
            Kanban is a project management tool that helps you visualize your
            work, limit work-in-progress, and maximize efficiency.
          </h3>
          <div className='buttons'>
            <Link to='/register' className='btn btn-landing'>
              Register
            </Link>
            <Link to='/login' className='btn btn-landing'>
              Login / Demo User
            </Link>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}
export default Landing
