// Model types

export type BoardModel = {
  _id?: string
  name: string
  columns: (string | undefined)[]
}

export type ColumnModel = {
  name: string
  tasks: string[]
  boardId: string
  _id?: string
}

export type TaskModel = {
  title: string
  description: string
  status: string
  columnId: string
  subtasks: SubtaskModel[]
  _id?: string
}

export type SubtaskModel = {
  title: string
  isCompleted: boolean
}
// State types for Redux

export type BoardsState = {
  boards: BoardModel[]
  status: 'idle' | 'loading' | 'succeeded' | 'failed'
  error: string | null
}

export type ColumnsState = {
  columns: ColumnModel[]
  status: 'idle' | 'loading' | 'succeeded' | 'failed'
  error: string | null
}

export type TasksState = {
  tasks: TaskModel[]
  status: 'idle' | 'loading' | 'succeeded' | 'failed'
  error: string | null
}

// Error type

export interface CustomError extends Error {
  response?: {
    data?: {
      msg?: string
    }
  }
}

// User type

export type User = {
  user: {
    id: string
    name: string
    email: string
    role: string
  }
}

// Props types

export type ColumnProps = {
  name: string
  columnId: string
}

export type FormRowProps = {
  type: string
  name: string
  labelText?: string
  value?: string
  autoComplete?: string
  placeholder?: string
}

// Data to send to the server

export type BoardData = {
  name: string
  columns: { name: string }[]
}
