import styled from 'styled-components'

const Wrapper = styled.nav`
  min-height: var(--nav-height);
  border-bottom: 0.5px solid var(--grey-primary);
  display: grid;
  align-items: center;

  .navbar {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 1rem 1rem;
    position: relative; /* Position par défaut pour les grands écrans */

    @media (max-width: 768px) {
      position: sticky; /* Navbar reste visible lors du défilement sur mobile */
      top: 0;
      background: var(--background-color);
      z-index: 1000;
    }

    .navbar-start {
      display: flex;
      align-items: center;
      gap: 1rem;

      .logo {
        @media (min-width: 768px) {
          display: none;
        }
      }

      .current-task {
        display: flex;
        align-items: center;
        gap: 0.25rem;
        cursor: pointer;

        h2 {
          font-size: 1.25rem;
          font-weight: 600;
          color: var(--text-color);
        }

        img {
          margin-top: 3px;
        }

        .chevron {
          @media (min-width: 768px) {
            display: none;
          }
        }
      }
    }

    .navbar-end {
      display: flex;
      align-items: center;
      gap: 1rem;
      right: 1rem;
      z-index: 100;

      .plus-icon {
        background-color: var(--purple-primary);
        height: 2.25rem;
        width: 3.75rem;
        border-radius: 1.25rem;
        position: relative;
        cursor: pointer;

        @media (min-width: 768px) {
          height: 2.5rem;
          width: 10rem;
          border-radius: 1.5rem;
          display: flex;
          justify-content: center;
          align-items: center;
          gap: 0.25rem;
        }

        :hover {
          background-color: var(--purple-secondary);
          transition: var(--transition);
        }

        .plus {
          position: absolute;
          left: 50%;
          top: 50%;
          transform: translate(-50%, -63%);
          font-size: 2rem;
          font-weight: 600;
          color: var(--white-primary);

          @media (min-width: 768px) {
            position: static;
            transform: none;
            padding-bottom: 4px;
            font-size: 1rem;
          }
        }

        .plus-text {
          display: none;
          text-transform: capitalize;

          @media (min-width: 768px) {
            display: block;
            font-size: 1rem;
            font-weight: 600;
            color: var(--white-primary);
          }
        }
      }

      .ellipsis-container {
        position: relative;

        .ellipsis {
          cursor: pointer;
        }

        .dropdown {
          position: absolute;
          visibility: hidden;
          z-index: 100;
          top: 46px;
          right: 0;
          width: 10rem;
          display: flex;
          flex-direction: column;
          border: 0.5px solid var(--grey-primary);
          border-radius: 1rem;

          @media (min-width: 768px) {
            width: 12rem;
          }

          .btn-dropdown {
            background-color: var(--background-color);
            color: var(--text-color);
            padding: 0.5rem 1rem;
            cursor: pointer;
            width: 100%;
            font-size: 1rem;

            &:hover {
              background-color: var(--background-color-secondary);
            }
          }

          .btn-dropdown:first-child {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
            color: var(--grey-primary);
          }

          .btn-dropdown:last-child {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            color: var(--red-primary);
          }
        }

        .show-dropdown {
          visibility: visible;
        }
      }

      .logout-container {
        position: relative;

        .account-icon {
          cursor: pointer;
        }

        .dropdown {
          position: absolute;
          visibility: hidden;
          z-index: 100;
          top: 50px;
          right: 0;
          width: 7.75rem;
          display: flex;
          flex-direction: column;

          @media (min-width: 768px) {
            width: 14rem;
          }

          .btn-dropdown {
            background-color: var(--background-color);
            color: var(--text-color);
            padding: 0.5rem 1rem;
            border: 0.5px solid var(--grey-primary);
            border-radius: 0.25rem;
            cursor: pointer;
            width: 100%;
            font-size: 1rem;

            &:hover {
              background-color: var(--background-color-secondary);
            }
          }
        }

        .show-dropdown {
          visibility: visible;
        }
      }
    }
  }
`

export default Wrapper
