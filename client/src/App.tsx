import {
  DashboardLayout,
  Landing,
  Error,
  Login,
  Register,
  HomeLayout,
} from './pages'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'

// actions
import { action as registerAction } from './pages/Register'
import { action as loginAction } from './pages/Login'

//loaders
import { loader as dashboardLoader } from './pages/DashboardLayout'

const router = createBrowserRouter([
  {
    path: '/',
    element: <HomeLayout />,
    errorElement: <Error />,
    children: [
      {
        index: true,
        element: <Landing />,
      },
      {
        path: '/dashboard',
        element: <DashboardLayout />,
        loader: dashboardLoader,
      },
      {
        path: '/login',
        element: <Login />,
        action: loginAction,
      },
      {
        path: '/register',
        element: <Register />,
        action: registerAction,
      },
    ],
  },
])

const App = () => {
  return <RouterProvider router={router} />
}
export default App
