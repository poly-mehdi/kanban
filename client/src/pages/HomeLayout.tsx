import { Outlet } from 'react-router-dom'
import styled from 'styled-components'

const Wrapper = styled.div`
  -ms-overflow-style: none;
  scrollbar-width: none;

  ::-webkit-scrollbar {
    display: none;
  }
`

const HomeLayout = () => {
  return (
    <Wrapper>
      <Outlet />
    </Wrapper>
  )
}
export default HomeLayout
