import { createSlice } from '@reduxjs/toolkit'

type BigSidebarState = {
  isOpen: boolean
}

const initializeBigSidebar = (): boolean => {
  const storedValue = localStorage.getItem('bigSidebar')
  return storedValue ? JSON.parse(storedValue) : true
}

const initialState: BigSidebarState = {
  isOpen: initializeBigSidebar(),
}

const bigSidebarSlice = createSlice({
  name: 'bigSidebar',
  initialState,
  reducers: {
    toggleBigSidebar: (state) => {
      state.isOpen = !state.isOpen
      localStorage.setItem('bigSidebar', JSON.stringify(state.isOpen))
      console.log('switch')
    },
  },
})

export const { toggleBigSidebar } = bigSidebarSlice.actions

export default bigSidebarSlice.reducer
