import styled from 'styled-components'

const Wrapper = styled.div`
  height: fit-content;

  .column-title {
    font-weight: 600;
    color: var(--grey-primary);
    margin-bottom: 1rem;
    text-transform: uppercase;
    height: 2.5rem;
  }

  .task-container {
    display: flex;
    flex-direction: column;
    gap: 1rem;

    max-height: calc(100vh - var(--nav-height) - 2.5rem - 5rem);
    overflow-y: auto;

    .task {
      background-color: var(--background-color);
      border-radius: 0.5rem;
      box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, 0.1);
      padding: 1.5rem 1rem;
      transition: all 0.25s ease-in-out;
      cursor: pointer;

      &:hover {
        transform: translateY(-0.25rem);
        box-shadow: 0 0.25rem 0.5rem rgba(0, 0, 0, 0.1);

        .task-title {
          color: var(--purple-primary);
        }
      }

      .task-title {
        font-weight: 700;
        color: var(--text-color);
        margin-bottom: 0.5rem;
      }

      .subtasks-counter {
        font-size: 0.75rem;
        color: var(--grey-primary);
      }
    }
  }
`

export default Wrapper
