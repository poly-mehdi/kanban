import Wrapper from '../assets/wrappers/AddBoard'
import { RxCross2 } from 'react-icons/rx'
import { useAppSelector } from '../hooks'
import { BoardData, ColumnModel } from '../utils/types'
import { toast } from 'react-toastify'
import customFetch from '../utils/customFetch'
import { CustomError } from '../utils/types'
import { useState } from 'react'
import { useAppDispatch } from '../hooks'
import { closeAddBoard } from '../features/dialog/addBoardSlice'
import { addReduxBoard } from '../features/boards/boardsSlice'
import { addColumns } from '../features/columns/columnsSlice'
import { v4 as uuid } from 'uuid'
import { setCurrentBoard } from '../features/currentBoard/currentBoardSlice'

const AddBoard = () => {
  // dispatch
  const dispatch = useAppDispatch()

  // state dialog
  const isAddBoardOpen = useAppSelector((state) => state.addBoardState.isOpen)
  const currentBoardId = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )

  // form state
  const [name, setName] = useState('')
  const [columns, setColumns] = useState<ColumnModel[]>([
    { name: '', _id: uuid(), boardId: currentBoardId, tasks: [] },
    { name: '', _id: uuid(), boardId: currentBoardId, tasks: [] },
  ])
  const addColumn = () => {
    const id = uuid()
    setColumns([
      ...columns,
      { name: '', _id: id, boardId: currentBoardId, tasks: [] },
    ])
  }
  const removeColumn = (index: number) =>
    setColumns(columns.filter((_, i) => i !== index))

  const handleColumnChange = (index: number, value: string) => {
    const newColumns = [...columns]
    newColumns[index].name = value
    setColumns(newColumns)
  }

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    const boardData: BoardData = {
      name,
      columns: columns.map((column) => ({ name: column.name })),
    }

    try {
      const response = await customFetch.post('/boards', boardData)
      const board = response.data.updatedBoard
      const columns = response.data.createdColumns
      dispatch(addReduxBoard(board))
      toast.success('Board created successfully')
      dispatch(addColumns(columns))
      dispatch(setCurrentBoard(board._id))
      dispatch(closeAddBoard())
      setName('')
      setColumns([])
    } catch (error) {
      const customError = error as CustomError
      toast.error(customError.response?.data?.msg || 'An error occurred')
      throw error
    }
  }

  return (
    <Wrapper>
      <div
        className={isAddBoardOpen ? 'task-display show-task' : 'task-display '}
      >
        <div className='content'>
          <form
            onSubmit={handleSubmit}
            method='post'
            className='task-container'
          >
            <div className='task-title'>
              <h2>Add new board</h2>
              <RxCross2
                size={24}
                className='title-cross'
                onClick={() => {
                  dispatch(closeAddBoard())
                  setName('')
                  setColumns([])
                }}
              />
            </div>
            <div className='form-row'>
              <label htmlFor='board-name' className='form-label'>
                Name
              </label>
              <input
                type='text'
                id='board-name'
                name='name'
                className='form-input'
                placeholder='Board Name'
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </div>
            <label htmlFor='subtasks'>board columns</label>
            <div className='subtasks-container'>
              {columns.map((column, index) => (
                <div className='subtask-container' key={index}>
                  <input
                    type='text'
                    className='subtask'
                    placeholder='e.g. To do'
                    value={column.name}
                    onChange={(e) => handleColumnChange(index, e.target.value)}
                    required
                  />
                  <RxCross2
                    className='cross'
                    size={24}
                    onClick={() => removeColumn(index)}
                  />
                </div>
              ))}
              <button type='button' className='btn btn-add' onClick={addColumn}>
                +Add new column
              </button>
            </div>
            <button type='submit' className='btn btn-create'>
              Create new board
            </button>
          </form>
        </div>
      </div>
    </Wrapper>
  )
}
export default AddBoard
