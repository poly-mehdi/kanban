import { useAppDispatch } from '../hooks'
import {
  toggleSmallSidebar,
  closeSmallSidebar,
} from '../features/sidebar/smallSidebarSlice'
import { VscAccount } from 'react-icons/vsc'
import { useState, useContext, useEffect, useRef } from 'react'
import { DashboardContext } from '../pages/DashboardLayout'
import { openAddTask } from '../features/dialog/addTaskSlice'
import Wrapper from '../assets/wrappers/Navbar'
import logo from '../assets/images/logo-mobile.svg'
import chevronDown from '../assets/images/icon-chevron-down.svg'
import ellipsis from '../assets/images/icon-vertical-ellipsis.svg'
import { openDeleteBoard } from '../features/dialog/deleteBoardSlice'
import { openEditBoard } from '../features/dialog/editBoardSlice'
import { useAppSelector } from '../hooks'

type logoutUser = () => void

const Navbar = () => {
  const { logoutUser } = useContext(DashboardContext) as {
    logoutUser: logoutUser
  }
  const dispatch = useAppDispatch()
  const [showLogout, setShowLogout] = useState(false)
  const [showEllipsis, setShowEllipsis] = useState(false)
  const [, setIsSmallSidebarOpen] = useState(false)

  const ellipsisRef = useRef<HTMLDivElement>(null)
  const logoutRef = useRef<HTMLDivElement>(null)
  const smallSidebarRef = useRef<HTMLDivElement>(null)

  const currentBoard = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )
  const boards = useAppSelector((state) => state.boardsState.boards)
  const currentBoardData = boards.find((board) => board._id === currentBoard)
  const name = currentBoardData?.name ?? ''

  const handleToggleSmallSidebar = () => {
    setShowLogout(false)
    setShowEllipsis(false)
    setIsSmallSidebarOpen((prev) => !prev)
    dispatch(toggleSmallSidebar())
  }

  const handleToggleEllipsis = () => {
    setShowLogout(false)
    setIsSmallSidebarOpen(false)
    dispatch(closeSmallSidebar())
    setShowEllipsis((prev) => !prev)
  }

  const handleToggleLogout = () => {
    setShowEllipsis(false)
    setIsSmallSidebarOpen(false)
    dispatch(closeSmallSidebar())
    setShowLogout((prev) => !prev)
  }

  const handleClickOutside = (event: MouseEvent) => {
    if (
      ellipsisRef.current &&
      !ellipsisRef.current.contains(event.target as Node) &&
      logoutRef.current &&
      !logoutRef.current.contains(event.target as Node) &&
      smallSidebarRef.current &&
      !smallSidebarRef.current.contains(event.target as Node)
    ) {
      setShowEllipsis(false)
      setShowLogout(false)
      setIsSmallSidebarOpen(false)
      dispatch(closeSmallSidebar())
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true)
    return () => {
      document.removeEventListener('click', handleClickOutside, true)
    }
  }, [])

  return (
    <Wrapper>
      <div className='navbar'>
        <div className='navbar-start'>
          <img src={logo} alt='logo' className='logo' />
          <div className='current-task' onClick={handleToggleSmallSidebar}>
            <h2>{name}</h2>
            <img src={chevronDown} alt='chevron-down' className='chevron' />
          </div>
        </div>
        <div className='navbar-end'>
          <div className='plus-icon' onClick={() => dispatch(openAddTask())}>
            <span className='plus'>+</span>
            <p className='plus-text'>Add new task</p>
          </div>
          <div className='ellipsis-container' ref={ellipsisRef}>
            <img
              src={ellipsis}
              alt='ellipsis'
              className='ellipsis'
              onClick={handleToggleEllipsis}
            />
            <div
              className={showEllipsis ? 'dropdown show-dropdown' : 'dropdown'}
            >
              <button
                className='btn btn-dropdown'
                onClick={() => {
                  dispatch(openEditBoard())
                  setShowEllipsis(false)
                }}
              >
                edit board
              </button>
              <button
                className='btn btn-dropdown'
                onClick={() => {
                  dispatch(openDeleteBoard())
                  setShowEllipsis(false)
                }}
              >
                delete board
              </button>
            </div>
          </div>
          <div className='logout-container' ref={logoutRef}>
            <VscAccount
              size={28}
              fill='#635fc7'
              className='account-icon'
              onClick={handleToggleLogout}
            />
            <div className={showLogout ? 'dropdown show-dropdown' : 'dropdown'}>
              <button className='btn btn-dropdown' onClick={logoutUser}>
                logout
              </button>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}

export default Navbar
