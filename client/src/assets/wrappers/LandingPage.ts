import styled from 'styled-components'

const Wrapper = styled.main`
  .logo {
    display: flex;
    align-items: center;
    width: var(--fluid-width);
    margin: 0 auto;
    height: var(--nav-height);
    max-width: var(--max-width);

    img {
      width: 2.5rem;
      margin-right: 0.5rem;
    }
    p {
      font-size: 2rem;
      font-weight: bold;
      color: var(--purple-primary);
    }
  }

  .landing {
    min-height: calc(100vh - var(--nav-height));
    display: grid;
    align-items: center;

    .info {
      display: flex;
      flex-direction: column;
      gap: 2rem;

      .title {
        font-size: 2rem;
        font-weight: bold;

        span {
          color: var(--purple-primary);
        }
      }

      .description {
        color: var(--grey-primary);
        font-weight: 400;
        text-transform: none;
        line-height: 1.5;
      }

      .buttons {
        display: flex;
        gap: 1.5rem;
        .btn-landing {
          background-color: var(--purple-primary);
          color: var(--white-primary);

          &:hover {
            background-color: var(--purple-secondary);
          }
        }
      }
    }
  }
`

export default Wrapper
