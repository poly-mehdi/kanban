import Wrapper from '../assets/wrappers/Column'
import { ColumnProps } from '../utils/types'
import { useAppDispatch, useAppSelector } from '../hooks'
import { TaskModel } from '../utils/types'
import { Key } from 'react'
import { setCurrentTask } from '../features/currentBoard/currentTaskSlice'
import { openDisplayTask } from '../features/dialog/displayTaskSlice'

const Column = ({ name, columnId }: ColumnProps) => {
  const tasks = useAppSelector((state) => state.tasksState.tasks)
  const currentTasks = tasks.filter((task) => task.columnId === columnId)
  const taskNumber = currentTasks.length

  const dispatch = useAppDispatch()

  return (
    <Wrapper>
      <h2 className='column-title'>{`${name} (${taskNumber})`}</h2>
      <div className='task-container'>
        {currentTasks?.map((task: TaskModel, index: Key | null | undefined) => (
          <div
            key={index}
            className='task'
            onClick={() => {
              dispatch(setCurrentTask(task._id!))
              dispatch(openDisplayTask())
            }}
          >
            <h3 className='task-title'>{task.title}</h3>
            {
              <p className='subtasks-counter'>
                {task.subtasks?.length === 0
                  ? 'No subtasks'
                  : `${
                      task.subtasks?.filter((subtask) => subtask.isCompleted)
                        .length
                    } of ${task.subtasks?.length} subtasks`}
              </p>
            }
          </div>
        ))}
      </div>
    </Wrapper>
  )
}
export default Column
