import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit'
import customFetch from '../../utils/customFetch'
import { ColumnModel, ColumnsState } from '../../utils/types'

export const fetchColumns = createAsyncThunk<ColumnModel[]>(
  'columns/fetchColumns',
  async () => {
    const response = await customFetch.get('/columns')
    return response.data.columns
  }
)

const initialState: ColumnsState = {
  columns: [],
  status: 'idle',
  error: null,
}

const columnsSlice = createSlice({
  name: 'columns',
  initialState,
  reducers: {
    addColumn: (state, action: PayloadAction<ColumnModel>) => {
      state.columns.push(action.payload)
    },
    addColumns: (state, action: PayloadAction<ColumnModel[]>) => {
      action.payload.forEach((column) => {
        if (!state.columns.find((col) => col._id === column._id)) {
          state.columns.push(column)
        }
      })
    },
    updateColumn: (state, action: PayloadAction<ColumnModel>) => {
      const index = state.columns.findIndex(
        (column) => column._id === action.payload._id
      )
      if (index !== -1) {
        state.columns[index] = action.payload
      }
    },
    deleteColumn: (state, action: PayloadAction<{ id: string }>) => {
      state.columns = state.columns.filter(
        (column) => column._id !== action.payload.id
      )
    },
    deleteColumns: (state, action: PayloadAction<string[]>) => {
      if (action.payload.length === 0) return
      state.columns = state.columns.filter(
        (column) => !action.payload.includes(column._id!)
      )
    },
    deleteColumnsByBoard: (state, action: PayloadAction<string>) => {
      state.columns = state.columns.filter(
        (column) => column.boardId !== action.payload
      )
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchColumns.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchColumns.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.columns = action.payload
      })
      .addCase(fetchColumns.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.error.message || null
      })
  },
})

export const {
  addColumn,
  addColumns,
  updateColumn,
  deleteColumn,
  deleteColumns,
  deleteColumnsByBoard,
} = columnsSlice.actions

export default columnsSlice.reducer
