import styled from 'styled-components'

const Wrapper = styled.main<{ $isSidebarOpen: boolean }>`
  display: grid;
  grid-template-columns: 1fr;

  @media (min-width: 768px) {
    grid-template-columns: auto 1fr;
  }

  .dashboard-page {
    background-color: var(--background-color-secondary);
    position: relative;
    max-width: 100vw;

    @media (min-width: 768px) {
      max-width: ${(props) =>
        props.$isSidebarOpen ? 'calc(100vw - 300px)' : '100vw'};
    }

    .open-sidebar {
      display: none;
      position: absolute;
      left: 0;
      bottom: 2rem;
      background-color: var(--purple-primary);
      height: 3rem;
      width: 3.5rem;
      border-top-right-radius: 2rem;
      border-bottom-right-radius: 2rem;
      z-index: 999;
      cursor: pointer;

      &:hover {
        opacity: 0.8;
      }

      @media (min-width: 768px) {
        display: grid;
        place-items: center;
      }
    }

    .close-sidebar {
      display: none;
    }
  }
`

export default Wrapper
