import { StatusCodes } from 'http-status-codes'
import User from '../models/userModel.js'
import Board from '../models/boardModel.js'

export const getCurrentUser = async (req, res) => {
  if (!req.user) {
    res.status(StatusCodes.OK).json({ userWithoutPassword: null })
    return
  }
  const user = await User.findOne({ _id: req.user.userId })
  const userWithoutPassword = user.toJSON()
  res.status(StatusCodes.OK).json({ userWithoutPassword })
}

export const getApplicationStats = async (req, res) => {
  const users = await User.countDocuments()
  const boards = await Board.countDocuments()
  res.status(StatusCodes.OK).json({ users, boards })
}

export const updateUser = async (req, res) => {
  const updatedUser = await User.findByIdAndUpdate(req.user.userId, req.body)
  res.status(StatusCodes.OK).json({ msg: 'user updated' })
}
