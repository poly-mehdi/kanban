import { configureStore } from '@reduxjs/toolkit'
import themeReducer from './features/theme/themeSlice'
import bigSidebarReducer from './features/sidebar/bigSidebarSlice'
import smallSidebarReducer from './features/sidebar/smallSidebarSlice'
import currentBoardReducer from './features/currentBoard/currentBoardSlice'
import addBoardReducer from './features/dialog/addBoardSlice'
import boardsReducer from './features/boards/boardsSlice'
import columnsReducer from './features/columns/columnsSlice'
import tasksReducer from './features/tasks/tasksSlice'
import addTaskReducer from './features/dialog/addTaskSlice'
import currentTaskReducer from './features/currentBoard/currentTaskSlice'
import displayTaskReducer from './features/dialog/displayTaskSlice'
import deleteBoardReducer from './features/dialog/deleteBoardSlice'
import deleteTaskReducer from './features/dialog/deleteTaskSlice'
import editTaskReducer from './features/dialog/editTaskSlice'
import editBoardReducer from './features/dialog/editBoardSlice'
// ...

export const store = configureStore({
  reducer: {
    themeState: themeReducer,
    bigSidebarState: bigSidebarReducer,
    smallSidebarState: smallSidebarReducer,
    currentBoardState: currentBoardReducer,
    currentTaskState: currentTaskReducer,
    addBoardState: addBoardReducer,
    addTaskState: addTaskReducer,
    boardsState: boardsReducer,
    columnsState: columnsReducer,
    tasksState: tasksReducer,
    displayTaskState: displayTaskReducer,
    deleteBoardState: deleteBoardReducer,
    deleteTaskState: deleteTaskReducer,
    editTaskState: editTaskReducer,
    editBoardState: editBoardReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export type ReduxStore = {
  getState: () => RootState
  dispatch: AppDispatch
}
