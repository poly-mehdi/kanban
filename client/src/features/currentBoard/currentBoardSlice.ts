import { createSlice } from '@reduxjs/toolkit'

type CurrentBoardState = {
  currentBoard: string
}

const initializeCurrentBoard = (): string => {
  const storedValue = localStorage.getItem('currentBoard')
  return storedValue !== null && storedValue !== 'undefined'
    ? JSON.parse(storedValue)
    : ''
}

const initialState: CurrentBoardState = {
  currentBoard: initializeCurrentBoard(),
}

const currentBoardSlice = createSlice({
  name: 'currentBoard',
  initialState,
  reducers: {
    setCurrentBoard: (state, action) => {
      if (action.payload !== undefined) {
        state.currentBoard = action.payload
        localStorage.setItem('currentBoard', JSON.stringify(state.currentBoard))
      }
    },
  },
})

export const { setCurrentBoard } = currentBoardSlice.actions

export default currentBoardSlice.reducer
