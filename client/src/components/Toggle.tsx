import Wrapper from '../assets/wrappers/Toggle'
import { useAppDispatch, useAppSelector } from '../hooks'
import { lightTheme, darkTheme } from '../assets/images'
import { switchTheme } from '../features/theme/themeSlice'

const Toggle = () => {
  const dispatch = useAppDispatch()
  const { theme } = useAppSelector((state) => state.themeState)

  return (
    <Wrapper>
      <img src={lightTheme} alt='light' />
      <div className='theme-switch' onClick={() => dispatch(switchTheme())}>
        <div className={`switch ${theme === 'dark' ? 'active-2' : ''}`}></div>
      </div>
      <img src={darkTheme} alt='dark' />
    </Wrapper>
  )
}
export default Toggle
