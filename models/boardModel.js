import mongoose from 'mongoose'
import { ColumnSchema } from './columnModel.js'

const BoardSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    // columns: [ColumnSchema],
    columns: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Column' }],
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  {
    timestamps: true,
  }
)

export default mongoose.model('Board', BoardSchema)
