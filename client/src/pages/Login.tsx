import Wrapper from '../assets/wrappers/LoginAndRegisterPage'
import {
  Form,
  Link,
  redirect,
  useNavigate,
  useNavigation,
} from 'react-router-dom'
import FormRow from '../components/FormRow'
import type { ActionFunction } from 'react-router'
import customFetch from '../utils/customFetch'
import { toast } from 'react-toastify'
import { CustomError } from '../utils/types'

export const action: ActionFunction = async ({ request }) => {
  const formData: FormData = await request.formData()
  const data = Object.fromEntries(formData)

  try {
    await customFetch.post('/auth/login', data)
    toast.success('Login successful')
    return redirect('/dashboard')
  } catch (error) {
    const customError = error as CustomError
    toast.error(customError.response?.data?.msg || 'An error occurred')
    return customError
  }
}

const Login = () => {
  const navigation = useNavigation()
  const isSubmitting = navigation.state === 'submitting'

  const navigate = useNavigate()

  const loginDemoUser = async () => {
    const data = {
      email: 'test@test.com',
      password: 'Secret123',
    }
    try {
      await customFetch.post('/auth/login', data)
      toast.success('Login successful')
      navigate('/dashboard')
    } catch (error) {
      const customError = error as CustomError
      toast.error(customError.response?.data?.msg || 'An error occurred')
      return customError
    }
  }

  return (
    <Wrapper>
      <Form method='post' className='form'>
        <h1>Login</h1>
        <FormRow type='email' name='email' autoComplete='email' />
        <FormRow
          type='password'
          name='password'
          autoComplete='current-password'
        />
        <button type='submit' className='btn-submit' disabled={isSubmitting}>
          {isSubmitting ? 'submitting...' : 'submit'}
        </button>
        <button type='button' className='btn-guest' onClick={loginDemoUser}>
          guest user
        </button>
        <p>
          Not a member yet?
          <Link to='/register'>Register</Link>
        </p>
      </Form>
    </Wrapper>
  )
}
export default Login
