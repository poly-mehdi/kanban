import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit'
import customFetch from '../../utils/customFetch'
import { TaskModel, TasksState } from '../../utils/types'

export const fetchTasks = createAsyncThunk<TaskModel[]>(
  'tasks/fetchTasks',
  async () => {
    const response = await customFetch.get('/tasks')
    return response.data.tasks
  }
)

const initialState: TasksState = {
  tasks: [],
  status: 'idle',
  error: null,
}

const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    addTask: (state, action: PayloadAction<TaskModel>) => {
      state.tasks.push(action.payload)
    },
    updateTaskLocally: (state, action: PayloadAction<TaskModel>) => {
      const index = state.tasks.findIndex(
        (task) => task._id === action.payload._id
      )
      if (index !== -1) {
        state.tasks[index] = action.payload
      }
    },
    deleteReduxTask: (state, action: PayloadAction<string>) => {
      state.tasks = state.tasks.filter((task) => task._id !== action.payload)
    },
    deleteTasksByColumns: (
      state,
      action: PayloadAction<(string | undefined)[]>
    ) => {
      state.tasks = state.tasks.filter(
        (task) => !action.payload.includes(task.columnId)
      )
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchTasks.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchTasks.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.tasks = action.payload
      })
      .addCase(fetchTasks.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.error.message || null
      })
  },
})

export const {
  addTask,
  updateTaskLocally,
  deleteReduxTask,
  deleteTasksByColumns,
} = tasksSlice.actions

export default tasksSlice.reducer
