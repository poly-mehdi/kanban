import styled from 'styled-components'

const Wrapper = styled.aside`
  display: none;

  @media (min-width: 768px) {
    display: block;
    border-right: 0.5px solid var(--grey-primary);

    .sidebar-container {
      min-height: 100vh;
      height: 100%;
      width: 300px;
      margin-left: -300px;
      transition: margin-left 0.3s ease-in-out;
      padding-bottom: 2rem;

      .content {
        position: sticky;
        top: 0;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        height: 100%;

        .sidebar-top {
          .logo {
            display: flex;
            align-items: center;
            height: 6rem;
            display: flex;
            align-items: center;
            padding-left: 3.5rem;

            img {
              width: 2.5rem;
              margin-right: 0.5rem;
            }
            p {
              font-size: 2rem;
              font-weight: bold;
              color: var(--purple-primary);
            }
          }

          .boards-container {
            letter-spacing: 3px;
            padding-top: 2rem;

            .boards-title {
              text-transform: uppercase;
              color: var(--grey-primary);
              font-size: 0.825rem;
              font-weight: 600;
              padding-left: 2.5rem;
            }

            .boards {
              display: flex;
              flex-direction: column;
              gap: 0.5rem;
              margin: 2rem 0;
              width: 275px;
              max-height: calc(100vh - 18rem - var(--nav-height));
              overflow-y: auto;

              .board {
                display: flex;
                align-items: center;
                gap: 0.5rem;
                cursor: pointer;
                padding-left: 2.5rem;
                padding-top: 1rem;
                padding-bottom: 1rem;

                p {
                  text-transform: capitalize;
                  letter-spacing: normal;
                  font-size: 1rem;
                  font-weight: 600;
                  color: var(--grey-primary);
                }
              }
              .active {
                background-color: var(--purple-primary);
                font-weight: bold;
                border-top-right-radius: 2.25rem;
                border-bottom-right-radius: 2.25rem;
                p {
                  color: var(--white-primary);
                }
              }
            }

            .boards-new {
              display: flex;
              align-items: center;
              gap: 0.5rem;
              cursor: pointer;
              padding-left: 2.5rem;

              p {
                text-transform: capitalize;
                color: var(--purple-primary);
                font-size: 1rem;
                letter-spacing: normal;
              }
            }
          }
        }

        .sidebar-bottom {
          display: flex;
          flex-direction: column;
          padding-bottom: 0rem;
          gap: 2rem;
          height: 6rem;

          .sidebar-switch {
            display: flex;
            align-items: center;
            gap: 0.5rem;
            cursor: pointer;
            padding-left: 1.5rem;

            p {
              text-transform: capitalize;
              color: var(--grey-primary);
              font-size: 1rem;
              letter-spacing: normal;
            }
          }
        }
      }
    }

    .show-sidebar {
      margin-left: 0;
    }
  }
`

export default Wrapper
