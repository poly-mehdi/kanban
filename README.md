# Frontend Mentor - Kanban task management web app solution

This is a solution to the [Kanban task management web app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/kanban-task-management-web-app-wgQLt-HlbB). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Create, read, update, and delete boards and tasks
- Receive form validations when trying to create/edit boards and tasks
- Mark subtasks as complete and move tasks between columns
- Hide/show the board sidebar
- Toggle the theme between light/dark modes
- **Bonus**: Allow users to drag and drop tasks to change their status and re-order them in a column
- **Bonus**: Keep track of any changes, even after refreshing the browser (`localStorage` could be used for this if you're not building out a full-stack app)
- **Bonus**: Build this project as a full-stack application

### Screenshot

![](./design/kanban.png)

### Links

- Solution URL: [kanban](https://kanban-m95s.onrender.com/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Styled Components](https://styled-components.com/) - For styles
- [Redux](https://redux.js.org/) - State management for JavaScript applications
- [MongoDB](https://www.mongodb.com/) - NoSQL database
- [Express](https://expressjs.com/) - Web framework for Node.js
- [Node.js](https://nodejs.org/) - JavaScript runtime environment
- [Mongoose](https://mongoosejs.com/) - Object Data Modeling (ODM) for MongoDB

### What I learned

- How to effectively use TypeScript in a MERN stack project, improving type safety and code quality.

### Continued development

- Implementation of React Query to enhance data fetching and caching in the application.
- Implementing drag and drop functionality between tasks to improve user interaction and task management.

## Author

- Website - [Portfolio](https://mehdisehad.dev)
- Frontend Mentor - [@polymehdi](https://www.frontendmentor.io/profile/poly-mehdi)
