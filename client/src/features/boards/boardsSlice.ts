import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit'
import customFetch from '../../utils/customFetch'
import { BoardModel, BoardsState } from '../../utils/types'

export const fetchBoards = createAsyncThunk<BoardModel[]>(
  'boards/fetchBoards',
  async () => {
    const response = await customFetch.get('/boards')
    return response.data.boards
  }
)

const initialState: BoardsState = {
  boards: [],
  status: 'idle',
  error: null,
}

const boardsSlice = createSlice({
  name: 'boards',
  initialState,
  reducers: {
    addReduxBoard: (state, action: PayloadAction<BoardModel>) => {
      state.boards.push(action.payload)
    },
    updateBoard: (state, action: PayloadAction<BoardModel>) => {
      const index = state.boards.findIndex(
        (board) => board._id === action.payload._id
      )
      if (index !== -1) {
        state.boards[index] = action.payload
      }
    },
    deleteReduxBoard: (state, action: PayloadAction<string>) => {
      state.boards = state.boards.filter(
        (board) => board._id !== action.payload
      )
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchBoards.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchBoards.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.boards = action.payload
      })
      .addCase(fetchBoards.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.error.message || null
      })
  },
})

export const { addReduxBoard, updateBoard, deleteReduxBoard } =
  boardsSlice.actions

export default boardsSlice.reducer
