import Wrapper from '../assets/wrappers/Columns'
import Column from './Column'
import AddColumn from './AddColumn'
import { useAppDispatch, useAppSelector } from '../hooks'
import { openEditBoard } from '../features/dialog/editBoardSlice'
import { openAddBoard } from '../features/dialog/addBoardSlice'

const Columns = () => {
  const dispatch = useAppDispatch()
  const currentBoard = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )
  const columns = useAppSelector((state) => state.columnsState.columns)
  const currentColumns = columns.filter(
    (column) => column.boardId === currentBoard
  )
  const boards = useAppSelector((state) => state.boardsState.boards)

  return (
    <Wrapper>
      {boards.length === 0 ? (
        <div className='empty'>
          <section className='columns-container-empty'>
            <p className='text-empty'>Create a new board to get started.</p>
            <div className='btn-empty' onClick={() => dispatch(openAddBoard())}>
              <span className='plus'>+</span>
              <p className='plus-text'>Add new board</p>
            </div>
          </section>
        </div>
      ) : currentColumns.length === 0 ? (
        <div className='empty'>
          <section className='columns-container-empty'>
            <p className='text-empty'>
              The board is empty. Create a new column to get started.
            </p>
            <div
              className='btn-empty'
              onClick={() => dispatch(openEditBoard())}
            >
              <span className='plus'>+</span>
              <p className='plus-text'>Add new column</p>
            </div>
          </section>
        </div>
      ) : (
        <div className='columns-container'>
          <ul className='columns'>
            {currentColumns.map((column, index) => (
              <li key={index} className='column'>
                <Column name={column.name} columnId={column._id!} />
              </li>
            ))}
            <li className='column'>
              <AddColumn />
            </li>
          </ul>
        </div>
      )}
    </Wrapper>
  )
}

export default Columns
