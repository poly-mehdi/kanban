import Wrapper from '../assets/wrappers/EditBoard'
import { useAppDispatch, useAppSelector } from '../hooks'
import { RxCross2 } from 'react-icons/rx'
import { useEffect, useState } from 'react'
import { closeEditBoard } from '../features/dialog/editBoardSlice'
import { updateBoard } from '../features/boards/boardsSlice'
import { toast } from 'react-toastify'
import customFetch from '../utils/customFetch'
import { CustomError } from '../utils/types'
import { addColumns, deleteColumns } from '../features/columns/columnsSlice'

const EditBoard = () => {
  const dispatch = useAppDispatch()
  const isEditBoardOpen = useAppSelector((state) => state.editBoardState.isOpen)
  const currentBoardId = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )
  const boards = useAppSelector((state) => state.boardsState.boards)
  const columns = useAppSelector((state) => state.columnsState.columns)
  const currentBoard = boards.find((board) => board._id === currentBoardId)

  const [name, setName] = useState('')
  const [boardColumns, setBoardColumns] = useState<
    { name: string; _id?: string }[]
  >([])

  useEffect(() => {
    if (currentBoard && columns.length > 0) {
      setName(currentBoard.name)
      const currentColumns = columns.filter(
        (column) => column.boardId === currentBoardId
      )
      setBoardColumns(
        currentColumns.map((column) => ({ name: column.name, _id: column._id }))
      )
    }
  }, [currentBoard, currentBoardId, columns])

  const addColumn = () => {
    setBoardColumns([...boardColumns, { name: '' }])
  }

  const removeColumn = (index: number) => {
    setBoardColumns(boardColumns.filter((_, i) => i !== index))
  }

  const handleColumnChange = (index: number, value: string) => {
    const newColumns = [...boardColumns]
    newColumns[index].name = value
    setBoardColumns(newColumns)
  }

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      const response = await customFetch.patch(`/boards/${currentBoardId}`, {
        name,
        columns: boardColumns,
      })
      const { updatedBoard, updatedColumns, removedColumns } = response.data
      dispatch(updateBoard(updatedBoard))
      dispatch(addColumns(updatedColumns))
      dispatch(deleteColumns(removedColumns))
      toast.success('Board updated successfully')
      dispatch(closeEditBoard())
    } catch (error) {
      const customError = error as CustomError
      toast.error(customError.response?.data?.msg || 'An error occurred')
      throw error
    }
  }

  return (
    <Wrapper>
      <div
        className={isEditBoardOpen ? 'task-display show-task' : 'task-display'}
      >
        <div className='content'>
          <form className='task-container' onSubmit={handleSubmit}>
            <div className='task-title'>
              <h2>Edit board</h2>
              <RxCross2
                size={24}
                className='title-cross'
                onClick={() => dispatch(closeEditBoard())}
              />
            </div>
            <div className='form-row'>
              <label htmlFor='title' className='form-label'>
                Board name
              </label>
              <input
                type='text'
                id='title'
                name='title'
                placeholder='e.g. Web Design'
                className='form-input'
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>

            <label htmlFor='subtasks'>Board columns</label>
            <div className='subtasks-container'>
              {boardColumns.map((column, index) => (
                <div className='subtask-container' key={index}>
                  <input
                    type='text'
                    className='subtask'
                    placeholder='e.g. Make coffee'
                    value={column.name}
                    onChange={(e) => handleColumnChange(index, e.target.value)}
                  />
                  <RxCross2
                    className='cross'
                    size={24}
                    onClick={() => removeColumn(index)}
                  />
                </div>
              ))}
              <button className='btn btn-add' type='button' onClick={addColumn}>
                +Add new column
              </button>
            </div>
            <button type='submit' className='btn btn-create'>
              Save changes
            </button>
          </form>
        </div>
      </div>
    </Wrapper>
  )
}

export default EditBoard
