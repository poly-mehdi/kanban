import { Router } from 'express'
const router = Router()

import { createColumn, getAllColumns } from '../controllers/columnController.js'
import { checkForTestUser } from '../middleware/authMiddleware.js'

router.route('/').get(getAllColumns).post(checkForTestUser, createColumn)

export default router
