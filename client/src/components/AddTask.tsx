import Wrapper from '../assets/wrappers/AddTask'
import { chevronDown } from '../assets/images'
import { RxCross2 } from 'react-icons/rx'
import { useAppDispatch, useAppSelector } from '../hooks'
import { useEffect, useRef, useState } from 'react'
import { closeAddTask } from '../features/dialog/addTaskSlice'
import { CustomError, SubtaskModel, TaskModel } from '../utils/types'
import { toast } from 'react-toastify'
import customFetch from '../utils/customFetch'
import { addTask } from '../features/tasks/tasksSlice'
import { updateColumn } from '../features/columns/columnsSlice'

const AddTask = () => {
  const dispatch = useAppDispatch()
  const isAddTaskOpen = useAppSelector((state) => state.addTaskState.isOpen)
  const currentBoardId = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )
  const columns = useAppSelector((state) => state.columnsState.columns)
  const contentRef = useRef<HTMLDivElement>(null)

  const [selectedColumnId, setSelectedColumnId] = useState<string>('')
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [subtasks, setSubtasks] = useState<SubtaskModel[]>([
    { title: '', isCompleted: false },
    { title: '', isCompleted: false },
  ])
  const [status, setStatus] = useState('')
  const [availableStatuses, setAvailableStatuses] = useState<
    { id: string | undefined; name: string }[]
  >([])
  const [showDropdown, setShowDropdown] = useState(false)

  const addSubtask = () => {
    setSubtasks([...subtasks, { title: '', isCompleted: false }])
  }

  const removeSubtask = (index: number) =>
    setSubtasks(subtasks.filter((_, i) => i !== index))

  const handleSubtaskChange = (index: number, value: string) => {
    const newSubtasks = [...subtasks]
    newSubtasks[index].title = value
    setSubtasks(newSubtasks)
  }

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    const taskData: TaskModel = {
      title,
      description,
      status:
        columns.find((column) => column._id === selectedColumnId)?.name || '',
      columnId: selectedColumnId,
      subtasks,
    }

    try {
      const response = await customFetch.post('/tasks', taskData)
      const task = response.data.task
      toast.success('Task created successfully')
      dispatch(addTask(task))

      const column = columns.find((column) => column._id === selectedColumnId)
      if (column) {
        dispatch(
          updateColumn({ ...column, tasks: [...column.tasks, task._id] })
        )
      }

      dispatch(closeAddTask())
      setTitle('')
      setDescription('')
      setSubtasks([
        { title: '', isCompleted: false },
        { title: '', isCompleted: false },
      ])
      setSelectedColumnId('')
    } catch (error) {
      const customError = error as CustomError
      toast.error(customError.response?.data?.msg || 'An error occurred')
      throw error
    }
  }

  useEffect(() => {
    const currentColumns = columns.filter(
      (column) => column.boardId === currentBoardId
    )
    const statuses = currentColumns.map((column) => ({
      id: column._id,
      name: column.name,
    }))
    setAvailableStatuses(statuses)
    if (statuses.length > 0) {
      setSelectedColumnId(statuses[0].id || '')
      setStatus(statuses[0].name || '')
    }
  }, [columns, currentBoardId])

  useEffect(() => {
    if (isAddTaskOpen && contentRef.current) {
      contentRef.current.scrollTop = 0
    }
  }, [isAddTaskOpen])

  const handleToggleDropdown = () => {
    setShowDropdown(!showDropdown)
  }

  const handleSelectStatus = (id: string, name: string) => {
    setSelectedColumnId(id)
    setStatus(name)
    setShowDropdown(false)
  }

  return (
    <Wrapper>
      <div
        className={isAddTaskOpen ? 'task-display show-task' : 'task-display'}
      >
        <div className='content' ref={contentRef}>
          <form className='task-container' onSubmit={handleSubmit}>
            <div className='task-title'>
              <h2>add new task</h2>
              <RxCross2
                size={24}
                className='title-cross'
                onClick={() => {
                  dispatch(closeAddTask())
                  setTitle('')
                  setDescription('')
                  setSubtasks([
                    { title: '', isCompleted: false },
                    { title: '', isCompleted: false },
                  ])
                }}
              />
            </div>
            <div className='form-row'>
              <label htmlFor='task-name' className='form-label'>
                Title
              </label>
              <input
                type='text'
                className='form-input'
                id='task-name'
                name='name'
                placeholder='e.g. Take coffee break'
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                required
              />
            </div>
            <div className='form-row'>
              <label htmlFor='description' className='form-label'>
                description
              </label>
              <textarea
                name='description'
                rows={4}
                placeholder="e.g. It's always good to take a break. This 15 minute break will  recharge the batteries a little."
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></textarea>
            </div>
            <label htmlFor='subtasks'>subtasks</label>
            <div className='subtasks-container'>
              {subtasks.map((subtask, index) => (
                <div className='subtask-container' key={index}>
                  <input
                    type='text'
                    className='subtask'
                    placeholder='e.g. Make coffee'
                    value={subtask.title}
                    onChange={(e) => handleSubtaskChange(index, e.target.value)}
                  />
                  <RxCross2
                    className='cross'
                    size={24}
                    onClick={() => removeSubtask(index)}
                  />
                </div>
              ))}
              <button
                className='btn btn-add'
                type='button'
                onClick={addSubtask}
              >
                +Add new subtask
              </button>
            </div>
            <label htmlFor='status'>status</label>

            <div className='status-container' onClick={handleToggleDropdown}>
              <p>{status}</p>
              <img src={chevronDown} alt='chevron-down' className='chevron' />
              {showDropdown && (
                <div className='dropdown'>
                  {availableStatuses.map((status) => (
                    <div
                      key={status.id}
                      className='dropdown-item'
                      onClick={() =>
                        handleSelectStatus(status.id || '', status.name)
                      }
                    >
                      {status.name}
                    </div>
                  ))}
                </div>
              )}
            </div>

            <button type='submit' className='btn btn-create'>
              Create task
            </button>
          </form>
        </div>
      </div>
    </Wrapper>
  )
}

export default AddTask
