import { useState, useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../hooks'
import { toast } from 'react-toastify'
import customFetch from '../utils/customFetch'
import Wrapper from '../assets/wrappers/EditTask'
import { RxCross2 } from 'react-icons/rx'
import { closeEditTask } from '../features/dialog/editTaskSlice'
import { updateTaskLocally } from '../features/tasks/tasksSlice'
import { SubtaskModel } from '../utils/types'

const EditTask = () => {
  const dispatch = useAppDispatch()

  // State for form fields
  const [title, setTitle] = useState<string>('')
  const [description, setDescription] = useState('')
  const [subtasks, setSubtasks] = useState<SubtaskModel[]>([])
  const [status, setStatus] = useState('Todo')

  // State dialog
  const isEditTaskOpen = useAppSelector((state) => state.editTaskState.isOpen)
  const currentTaskId = useAppSelector(
    (state) => state.currentTaskState.currentTask
  )
  const tasks = useAppSelector((state) => state.tasksState.tasks)
  const currentTask = tasks.find((task) => task._id === currentTaskId)

  // Initialize form fields when currentTask changes
  useEffect(() => {
    if (currentTask) {
      setTitle(currentTask.title)
      setDescription(currentTask.description)
      setSubtasks(currentTask.subtasks)
      setStatus(currentTask.status)
    }
  }, [currentTask])

  const handleSubtaskChange = (index: number, value: string) => {
    const newSubtasks = [...subtasks]
    newSubtasks[index].title = value
    setSubtasks(newSubtasks)
  }

  const handleAddSubtask = () => {
    setSubtasks([...subtasks, { title: '', isCompleted: false }])
  }

  const handleRemoveSubtask = (index: number) => {
    const newSubtasks = subtasks.filter((_, i) => i !== index)
    setSubtasks(newSubtasks)
  }

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      const updatedTask = { title, description, subtasks, status }
      const response = await customFetch.patch(
        `/tasks/${currentTaskId}`,
        updatedTask
      )
      console.log(response.data.updatedTask)

      dispatch(updateTaskLocally(response.data.updatedTask))
      dispatch(closeEditTask())
      toast.success('Task updated successfully')
    } catch (error) {
      toast.error('An error occurred')
    }
  }

  return (
    <Wrapper>
      <div
        className={isEditTaskOpen ? 'task-display show-task' : 'task-display'}
      >
        <div className='content'>
          <form className='task-container' onSubmit={handleSubmit}>
            <div className='task-title'>
              <h2>Edit Task</h2>
            </div>
            <div className='form-row'>
              <label htmlFor='task-title' className='form-label'>
                Name
              </label>
              <input
                type='text'
                id='task-title'
                name='title'
                className='form-input'
                placeholder='e.g. Take coffee break'
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                required
              />
            </div>
            <div className='form-row'>
              <label htmlFor='description' className='form-label'>
                Description
              </label>
              <textarea
                name='description'
                rows={4}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                placeholder="e.g. It's always good to take a break. This 15 minute break will recharge the batteries a little."
              ></textarea>
            </div>
            <label htmlFor='subtasks'>Subtasks</label>
            <div className='subtasks-container'>
              {subtasks.map((subtask, index) => (
                <div className='subtask-container' key={index}>
                  <input
                    type='text'
                    className='subtask'
                    value={subtask.title}
                    onChange={(e) => handleSubtaskChange(index, e.target.value)}
                    placeholder='e.g. Make coffee'
                  />
                  <RxCross2
                    className='cross'
                    size={24}
                    onClick={() => handleRemoveSubtask(index)}
                  />
                </div>
              ))}
              <button
                type='button'
                className='btn btn-add'
                onClick={handleAddSubtask}
              >
                +Add new subtask
              </button>
            </div>
            <label htmlFor='status'>Status</label>
            <div className='status-container'>
              <p>{status}</p>
            </div>
            <button type='submit' className='btn btn-create'>
              Save changes
            </button>
          </form>
        </div>
      </div>
    </Wrapper>
  )
}

export default EditTask
