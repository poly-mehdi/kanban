import { createSlice } from '@reduxjs/toolkit'

type CurrentTaskState = {
  currentTask: string
}

const initialState: CurrentTaskState = {
  currentTask: '',
}

const currentTaskSlice = createSlice({
  name: 'currentTask',
  initialState,
  reducers: {
    setCurrentTask: (state, action) => {
      if (action.payload !== undefined) {
        state.currentTask = action.payload
      }
    },
  },
})

export const { setCurrentTask } = currentTaskSlice.actions

export default currentTaskSlice.reducer
