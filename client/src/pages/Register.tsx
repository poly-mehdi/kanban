import { Form, Link, redirect, useNavigation } from 'react-router-dom'
import Wrapper from '../assets/wrappers/LoginAndRegisterPage'
import FormRow from '../components/FormRow'
import type { ActionFunction } from 'react-router'
import customFetch from '../utils/customFetch'
import { toast } from 'react-toastify'

interface CustomError extends Error {
  response?: {
    data?: {
      msg?: string
    }
  }
}

export const action: ActionFunction = async ({ request }) => {
  const formData: FormData = await request.formData()
  const data = Object.fromEntries(formData)

  try {
    await customFetch.post('/auth/register', data)
    toast.success('Registration successful')
    return redirect('/login')
  } catch (error) {
    const customError = error as CustomError
    toast.error(customError.response?.data?.msg || 'An error occurred')
    return customError
  }
}

const Register = () => {
  const navigation = useNavigation()
  const isSubmitting = navigation.state === 'submitting'
  return (
    <Wrapper>
      <Form method='post' className='form'>
        <h1>Register</h1>
        <FormRow type='text' name='name' />
        <FormRow type='text' name='lastName' labelText='last name' />
        <FormRow type='email' name='email' />
        <FormRow type='password' name='password' />
        <button type='submit' className='btn-submit' disabled={isSubmitting}>
          {isSubmitting ? 'submitting...' : 'submit'}
        </button>
        <p>
          Already a member ?<Link to='/login'>Login</Link>
        </p>
      </Form>
    </Wrapper>
  )
}
export default Register
