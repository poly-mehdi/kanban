import { Router } from 'express'
const router = Router()

import {
  getAllTasks,
  createTask,
  updateTask,
  deleteTask,
} from '../controllers/taskController.js'
import { validateIdParam } from '../middleware/validationMiddleware.js'
import { checkForTestUser } from '../middleware/authMiddleware.js'

router.route('/').get(getAllTasks).post(checkForTestUser, createTask)
router
  .route('/:id')
  .patch(checkForTestUser, updateTask)
  .delete(checkForTestUser, deleteTask)

export default router
