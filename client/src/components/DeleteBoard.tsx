import { toast } from 'react-toastify'
import Wrapper from '../assets/wrappers/DeleteBoard'
import { closeDeleteBoard } from '../features/dialog/deleteBoardSlice'
import { useAppDispatch, useAppSelector } from '../hooks'
import customFetch from '../utils/customFetch'
import { CustomError } from '../utils/types'
import { deleteReduxBoard } from '../features/boards/boardsSlice'
import { deleteColumnsByBoard } from '../features/columns/columnsSlice'
import { deleteTasksByColumns } from '../features/tasks/tasksSlice'

const DeleteBoard = () => {
  const dispatch = useAppDispatch()
  const isDeleteBoardOpen = useAppSelector(
    (state) => state.deleteBoardState.isOpen
  )
  const currentBoardId = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )
  const boards = useAppSelector((state) => state.boardsState.boards)
  const columns = useAppSelector((state) => state.columnsState.columns)
  const currentBoard = boards.find((board) => board._id === currentBoardId)

  const deleteBoard = async () => {
    try {
      await customFetch.delete(`/boards/${currentBoardId}`)

      // Delete columns and tasks associated with the board in Redux
      const columnsToDelete = columns
        .filter((column) => column.boardId === currentBoardId)
        .map((column) => column._id)

      dispatch(deleteTasksByColumns(columnsToDelete))
      dispatch(deleteColumnsByBoard(currentBoardId))
      dispatch(deleteReduxBoard(currentBoardId))

      dispatch(closeDeleteBoard())
      toast.success('Board deleted successfully')
    } catch (error) {
      const customError = error as CustomError
      toast.error(customError.response?.data?.msg || 'An error occurred')
      throw error
    }
  }

  return (
    <Wrapper>
      <div
        className={
          isDeleteBoardOpen ? 'board-delete show-delete' : 'board-delete'
        }
      >
        <div className='content'>
          <form className='container'>
            <div className='title'>
              <h2>Delete this board?</h2>
            </div>
            <p className='description'>
              {`Are you sure you want to delete the ‘${currentBoard?.name}’ board?
              This action will remove all columns and tasks and cannot be
              reversed.`}
            </p>
            <div className='btn-container'>
              <div className='btn btn-delete' onClick={deleteBoard}>
                Delete
              </div>
              <div
                className='btn btn-cancel'
                onClick={() => dispatch(closeDeleteBoard())}
              >
                Cancel
              </div>
            </div>
          </form>
        </div>
      </div>
    </Wrapper>
  )
}

export default DeleteBoard
