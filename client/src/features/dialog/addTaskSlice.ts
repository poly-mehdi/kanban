import { createSlice } from '@reduxjs/toolkit'

type AddTaskState = {
  isOpen: boolean
}

const initialState: AddTaskState = {
  isOpen: false,
}

const createTaskSlice = createSlice({
  name: 'createTask',
  initialState,
  reducers: {
    openAddTask: (state) => {
      state.isOpen = true
    },
    closeAddTask: (state) => {
      state.isOpen = false
    },
  },
})

export const { openAddTask, closeAddTask } = createTaskSlice.actions

export default createTaskSlice.reducer
