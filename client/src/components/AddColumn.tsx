import Wrapper from '../assets/wrappers/AddColumn'
import { openEditBoard } from '../features/dialog/editBoardSlice'
import { useAppDispatch } from '../hooks'

const AddColumn = () => {
  const dispatch = useAppDispatch()
  return (
    <Wrapper onClick={() => dispatch(openEditBoard())}>
      <div className='content'>
        <h1>+ new column</h1>
      </div>
    </Wrapper>
  )
}
export default AddColumn
