import { useRouteError, Link, isRouteErrorResponse } from 'react-router-dom'
import Wrapper from '../assets/wrappers/ErrorPage'

const Error = () => {
  // type:unknown
  const error: unknown = useRouteError()
  console.log(error)

  if (isRouteErrorResponse(error) && error.status === 404) {
    return (
      <Wrapper>
        <div>
          <h3>Ohh! page not found</h3>
          <p>We can't seem to find the page you're looking for</p>
          <Link to='/dashboard'>back home</Link>
        </div>
      </Wrapper>
    )
  }

  return (
    <Wrapper>
      <div>
        <h3>something went wrong</h3>
      </div>
    </Wrapper>
  )
}
export default Error
