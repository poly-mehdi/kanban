import { createSlice } from '@reduxjs/toolkit'

type SmallSidebarState = {
  isOpen: boolean
}

const initializeSmallSidebar = (): boolean => {
  const storedValue = localStorage.getItem('smallSidebar')
  return storedValue ? JSON.parse(storedValue) : false
}

const initialState: SmallSidebarState = {
  isOpen: initializeSmallSidebar(),
}

const smallSidebarSlice = createSlice({
  name: 'smallSidebar',
  initialState,
  reducers: {
    toggleSmallSidebar: (state) => {
      state.isOpen = !state.isOpen
      localStorage.setItem('smallSidebar', JSON.stringify(state.isOpen))
    },
    closeSmallSidebar: (state) => {
      state.isOpen = false
      localStorage.setItem('smallSidebar', JSON.stringify(state.isOpen))
    },
  },
})

export const { toggleSmallSidebar, closeSmallSidebar } =
  smallSidebarSlice.actions

export default smallSidebarSlice.reducer
