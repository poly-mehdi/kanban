import Task from '../models/taskModel.js'
import Column from '../models/columnModel.js'
import { StatusCodes } from 'http-status-codes'

export const getAllTasks = async (req, res) => {
  const tasks = await Task.find({ createdBy: req.user.userId })
  res.status(StatusCodes.OK).json({ tasks })
}

export const createTask = async (req, res) => {
  req.body.createdBy = req.user.userId
  const { title, description, status, columnId, subtasks } = req.body
  const task = await Task.create({
    title,
    description,
    status,
    columnId,
    subtasks,
    createdBy: req.user.userId,
  })

  // Add the task to the column
  await Column.findByIdAndUpdate(
    columnId,
    { $push: { tasks: task._id } },
    { new: true }
  )

  res.status(StatusCodes.CREATED).json({ task })
}

export const updateTask = async (req, res) => {
  const updatedTask = await Task.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  })

  res.status(StatusCodes.OK).json({ msg: 'task modified', updatedTask })
}

export const deleteTask = async (req, res) => {
  const removedTask = await Task.findByIdAndDelete(req.params.id)

  res.status(StatusCodes.OK).json({ msg: 'task deleted', task: removedTask })
}
