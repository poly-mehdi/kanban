import Wrapper from '../assets/wrappers/DashboardLayout'
import {
  BigSidebar,
  Navbar,
  Columns,
  SmallSidebar,
  TaskDisplay,
  AddTask,
  EditTask,
  AddBoard,
  EditBoard,
  DeleteBoard,
  DeleteTask,
} from '../components'
import { show } from '../assets/images'
import { toggleBigSidebar } from '../features/sidebar/bigSidebarSlice'
import { useAppDispatch, useAppSelector } from '../hooks'
import customFetch from '../utils/customFetch'
import { redirect, useNavigate } from 'react-router-dom'
import { createContext, useEffect } from 'react'
import { toast } from 'react-toastify'
import { fetchBoards } from '../features/boards/boardsSlice'
import { fetchTasks } from '../features/tasks/tasksSlice'
import { fetchColumns } from '../features/columns/columnsSlice'
import { setCurrentBoard } from '../features/currentBoard/currentBoardSlice'

const DashboardContext = createContext({})

export const loader = async () => {
  try {
    const { data } = await customFetch.get('/users/current-user')
    return { data }
  } catch (error) {
    return redirect('/')
  }
}

const DashboardLayout = () => {
  const isSidebarOpen = useAppSelector((state) => state.bigSidebarState.isOpen)
  const boards = useAppSelector((state) => state.boardsState.boards)
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const currentBoardId = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )

  const logoutUser = async () => {
    navigate('/')
    await customFetch.get('/auth/logout')
    toast.success('Logged out successfully')
  }

  useEffect(() => {
    dispatch(fetchBoards())
    dispatch(fetchColumns())
    dispatch(fetchTasks())
  }, [])

  useEffect(() => {
    if (boards.length > 0) {
      const storedCurrentBoard = localStorage.getItem('currentBoard')
      const currentBoardId = storedCurrentBoard
        ? JSON.parse(storedCurrentBoard)
        : boards[0]._id

      const boardExists = boards.some((board) => board._id === currentBoardId)

      if (boardExists) {
        dispatch(setCurrentBoard(currentBoardId))
      } else {
        dispatch(setCurrentBoard(boards[0]._id))
      }
    }
  }, [boards])

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [currentBoardId])

  return (
    <DashboardContext.Provider value={{ logoutUser }}>
      <Wrapper $isSidebarOpen={isSidebarOpen}>
        <BigSidebar />
        <SmallSidebar />
        <div>
          <TaskDisplay />
          <AddTask />
          <AddBoard />
          <EditTask />
          <EditBoard />
          <DeleteTask />
          <DeleteBoard />
          <Navbar />
          <div className='dashboard-page'>
            <div
              className={isSidebarOpen ? 'close-sidebar' : 'open-sidebar'}
              onClick={() => dispatch(toggleBigSidebar())}
            >
              <img src={show} alt='show' className='show-sidebar' />
            </div>
            <Columns />
          </div>
        </div>
      </Wrapper>
    </DashboardContext.Provider>
  )
}
export default DashboardLayout

export { DashboardContext }
