import { createSlice } from '@reduxjs/toolkit'

type DeleteTaskState = {
  isOpen: boolean
}

const initialState: DeleteTaskState = {
  isOpen: false,
}

const deleteTaskSlice = createSlice({
  name: 'deleteTask',
  initialState,
  reducers: {
    openDeleteTask: (state) => {
      state.isOpen = true
    },
    closeDeleteTask: (state) => {
      state.isOpen = false
    },
  },
})

export const { openDeleteTask, closeDeleteTask } = deleteTaskSlice.actions

export default deleteTaskSlice.reducer
