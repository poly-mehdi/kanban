import { toast } from 'react-toastify'
import Wrapper from '../assets/wrappers/DeleteTask'
import { closeDeleteTask } from '../features/dialog/deleteTaskSlice'
import { useAppDispatch, useAppSelector } from '../hooks'
import customFetch from '../utils/customFetch'
import { CustomError } from '../utils/types'
import { deleteReduxTask } from '../features/tasks/tasksSlice'

const DeleteTask = () => {
  const dispatch = useAppDispatch()
  const isDeleteTaskOpen = useAppSelector(
    (state) => state.deleteTaskState.isOpen
  )
  const currentTaskId = useAppSelector(
    (state) => state.currentTaskState.currentTask
  )
  const tasks = useAppSelector((state) => state.tasksState.tasks)
  const currentTask = tasks.find((task) => task._id === currentTaskId)

  const deleteTask = async () => {
    try {
      await customFetch.delete(`/tasks/${currentTaskId}`)
      dispatch(closeDeleteTask())
      dispatch(deleteReduxTask(currentTaskId))
      toast.success('Task deleted successfully')
    } catch (error) {
      const customError = error as CustomError
      toast.error(customError.response?.data?.msg || 'An error occurred')
      throw error
    }
  }

  return (
    <Wrapper>
      <div
        className={isDeleteTaskOpen ? 'task-delete show-delete' : 'task-delete'}
      >
        <div className='content'>
          <form className='container'>
            <div className='title'>
              <h2>Delete this task?</h2>
            </div>
            <p className='description'>
              {`Are you sure you want to delete the ‘${currentTask?.title}’ task and
              its subtasks? This action cannot be reversed.`}
            </p>
            <div className='btn-container'>
              <div className='btn btn-delete' onClick={deleteTask}>
                Delete
              </div>
              <div
                className='btn btn-cancel'
                onClick={() => dispatch(closeDeleteTask())}
              >
                Cancel
              </div>
            </div>
          </form>
        </div>
      </div>
    </Wrapper>
  )
}
export default DeleteTask
