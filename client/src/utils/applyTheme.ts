import { Theme } from '../features/theme/themeSlice'

export function applyTheme(theme: Theme) {
  const root = window.document.documentElement

  root.classList.remove('light', 'dark')

  if (theme === 'light' || theme === 'dark') {
    root.classList.add(theme)
  } else {
    console.error('Invalid theme:', theme)
    root.classList.add('light')
  }
}
