import styled from 'styled-components'

const Wrapper = styled.div`
  width: calc(300px - 2.5rem);
  height: 3rem;
  border-radius: 0.5rem;
  background-color: var(--background-color-secondary);
  margin: 0 auto;
  display: flex;
  gap: 2rem;
  align-items: center;
  justify-content: center;

  .theme-switch {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 6px;
    background-color: var(--purple-primary);
    padding: 4px 4px;
    border-radius: 2rem;
    cursor: pointer;
    width: 3rem;

    .switch {
      width: 1rem;
      height: 1rem;
      border-radius: 50%;
      background-color: #ffffff;
      transition: 0.2s all linear;
    }
    .active-2 {
      transform: translateX(calc(1.2rem + 4px));
    }
  }
`

export default Wrapper
