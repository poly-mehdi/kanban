import { createSlice } from '@reduxjs/toolkit'
import { applyTheme } from '../../utils'

export type Theme = 'dark' | 'light'

type ThemeState = {
  theme: Theme
}

const initializeTheme = (): Theme => {
  const theme = (localStorage.getItem('theme') as Theme) || 'light'
  applyTheme(theme)
  return theme
}

const initialState: ThemeState = {
  theme: initializeTheme(),
}

const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    switchTheme: (state) => {
      state.theme = state.theme === 'dark' ? 'light' : 'dark'
      applyTheme(state.theme)
      localStorage.setItem('theme', state.theme)
    },
  },
})

export const { switchTheme } = themeSlice.actions

export default themeSlice.reducer
