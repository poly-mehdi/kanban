import styled from 'styled-components'

const Wrapper = styled.aside`
  @media (min-width: 768px) {
    display: none;
  }

  .sidebar-container {
    position: fixed;
    top: calc(var(--nav-height));
    left: 0;
    background-color: rgba(0, 0, 0, 0.3);
    width: 100%;
    height: 100%;
    visibility: hidden;
    z-index: -1;
    opacity: 0;
    transition: var(--transition);

    .content {
      width: 75vw;
      margin: 0 auto;
      background-color: var(--background-color);
      margin-top: 1rem;
      padding: 1.5rem 0;
      border-radius: 0.75rem;

      .boards-container {
        letter-spacing: 3px;

        .boards-title {
          text-transform: uppercase;
          color: var(--grey-primary);
          font-size: 0.825rem;
          font-weight: 600;
          padding-left: 1rem;
        }

        .boards {
          display: flex;
          flex-direction: column;
          gap: 0.5rem;
          margin: 2rem 0;
          width: 95%;

          .board {
            display: flex;
            align-items: center;
            gap: 0.5rem;
            cursor: pointer;
            padding-left: 1rem;
            padding-top: 1rem;
            padding-bottom: 1rem;

            p {
              text-transform: capitalize;
              letter-spacing: normal;
              font-size: 1rem;
              font-weight: 600;
              color: var(--grey-primary);
            }
          }
          .active {
            background-color: var(--purple-primary);
            font-weight: bold;
            border-top-right-radius: 2.25rem;
            border-bottom-right-radius: 2.25rem;
            p {
              color: var(--white-primary);
            }
          }
        }

        .boards-new {
          display: flex;
          align-items: center;
          gap: 0.5rem;
          cursor: pointer;
          padding-left: 1rem;
          margin-bottom: 2rem;

          p {
            text-transform: capitalize;
            color: var(--purple-primary);
            font-size: 1rem;
            letter-spacing: normal;
          }
        }
      }
    }
  }
  .show-sidebar {
    z-index: 99;
    opacity: 1;
    visibility: visible;
  }
`
export default Wrapper
