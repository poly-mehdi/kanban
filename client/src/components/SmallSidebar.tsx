import Wrapper from '../assets/wrappers/SmallSidebar'
import { board as boardImage, boardPurple, boardWhite } from '../assets/images'
import Toggle from './Toggle'
import { useAppSelector, useAppDispatch } from '../hooks'
import { setCurrentBoard } from '../features/currentBoard/currentBoardSlice'
import { openAddBoard } from '../features/dialog/addBoardSlice'
import { toggleSmallSidebar } from '../features/sidebar/smallSidebarSlice'

const SmallSidebar = () => {
  const dispatch = useAppDispatch()
  const isSidebarOpen = useAppSelector(
    (state) => state.smallSidebarState.isOpen
  )
  const allBoards = useAppSelector((state) => state.boardsState.boards)
  const currentBoard = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )

  return (
    <Wrapper>
      <div
        className={
          isSidebarOpen ? 'sidebar-container show-sidebar' : 'sidebar-container'
        }
      >
        <div className='content'>
          <div className='boards-container'>
            <p className='boards-title'>{`all boards (${allBoards.length})`}</p>
            <div className='boards'>
              {allBoards.map((board) =>
                board._id === currentBoard ? (
                  <div
                    className='board active'
                    onClick={() => dispatch(setCurrentBoard(board._id))}
                    key={board._id}
                  >
                    <img src={boardWhite} alt='board' />
                    <p>{board.name}</p>
                  </div>
                ) : (
                  <div
                    key={board._id}
                    className='board'
                    onClick={() => {
                      dispatch(setCurrentBoard(board._id))
                      dispatch(toggleSmallSidebar())
                    }}
                  >
                    <img src={boardImage} alt='board' />
                    <p>{board.name}</p>
                  </div>
                )
              )}
            </div>
            {allBoards.length > 0 && (
              <div
                className='boards-new'
                onClick={() => {
                  dispatch(openAddBoard())
                  dispatch(toggleSmallSidebar())
                }}
              >
                <img src={boardPurple} alt='board' />
                <p>+Create new board</p>
              </div>
            )}
          </div>
          <Toggle />
        </div>
      </div>
    </Wrapper>
  )
}

export default SmallSidebar
