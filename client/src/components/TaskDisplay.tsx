import Wrapper from '../assets/wrappers/TaskDisplay'
import { vertical as ellipsis, chevronDown } from '../assets/images'
import { useAppSelector, useAppDispatch } from '../hooks'
import { closeDisplayTask } from '../features/dialog/displayTaskSlice'
import { RxCross2 } from 'react-icons/rx'
import { useState } from 'react'
import { openDeleteTask } from '../features/dialog/deleteTaskSlice'
import { openEditTask } from '../features/dialog/editTaskSlice'
import { updateTaskLocally } from '../features/tasks/tasksSlice'
import customFetch from '../utils/customFetch'
import { TaskModel } from '../utils/types'
import { toast } from 'react-toastify'

const TaskDisplay = () => {
  const [showEllipsis, setShowEllipsis] = useState(false)
  const [showStatusDropdown, setShowStatusDropdown] = useState(false)

  const dispatch = useAppDispatch()
  const tasks = useAppSelector((state) => state.tasksState.tasks)
  const currentTaskId = useAppSelector(
    (state) => state.currentTaskState.currentTask
  )
  const currentTask = tasks.find((task) => task._id === currentTaskId)
  const isDisplayTaskOpen = useAppSelector(
    (state) => state.displayTaskState.isOpen
  )
  const columns = useAppSelector((state) => state.columnsState.columns)
  const currentBoardId = useAppSelector(
    (state) => state.currentBoardState.currentBoard
  )
  const currentColumns = columns.filter(
    (column) => column.boardId === currentBoardId
  )

  // Map column IDs to names
  const columnIdToName: { [key: string]: string } = currentColumns.reduce(
    (acc, column) => {
      if (column._id) acc[column._id] = column.name
      return acc
    },
    {} as { [key: string]: string }
  )

  const onClose = () => {
    dispatch(closeDisplayTask())
    setShowEllipsis(false)
  }

  const handleToggleEllipsis = () => {
    setShowEllipsis((prev) => !prev)
  }

  const handleToggleSubtask = async (index: number) => {
    if (currentTask) {
      const updatedSubtasks = currentTask.subtasks.map((subtask, i) =>
        i === index
          ? { ...subtask, isCompleted: !subtask.isCompleted }
          : subtask
      )
      const updatedTask: TaskModel = {
        ...currentTask,
        subtasks: updatedSubtasks,
      }

      try {
        const response = await customFetch.patch(
          `/tasks/${currentTask._id}`,
          updatedTask
        )
        dispatch(updateTaskLocally(response.data.updatedTask))
        toast.success('Task updated successfully')
      } catch (error) {
        toast.error('An error occurred while updating the task')
      }
    }
  }

  const handleStatusChange = async (statusId: string) => {
    if (currentTask) {
      const updatedTask: TaskModel = {
        ...currentTask,
        columnId: statusId,
        status: columnIdToName[statusId],
      }

      try {
        const response = await customFetch.patch(
          `/tasks/${currentTask._id}`,
          updatedTask
        )
        dispatch(updateTaskLocally(response.data.updatedTask))
        toast.success('Task status updated successfully')
      } catch (error) {
        toast.error('An error occurred while updating the task status')
      }
    }
    setShowStatusDropdown(false)
  }

  if (!currentTask) {
    return null // If no task is selected, don't render anything
  }

  return (
    <Wrapper>
      <div
        className={
          isDisplayTaskOpen ? 'task-display show-task' : 'task-display'
        }
      >
        <div className='content'>
          <div className='task-container'>
            <div className='task-title'>
              <h2>{currentTask.title}</h2>
              <div className='task-title-end'>
                <div
                  className='ellipsis-container'
                  onClick={handleToggleEllipsis}
                >
                  <img src={ellipsis} alt='ellipsis' className='ellipsis' />
                  <div
                    className={
                      showEllipsis ? 'dropdown show-dropdown' : 'dropdown'
                    }
                  >
                    <button
                      className='btn btn-dropdown'
                      onClick={() => {
                        dispatch(closeDisplayTask())
                        dispatch(openEditTask())
                      }}
                    >
                      edit task
                    </button>
                    <button
                      className='btn btn-dropdown'
                      onClick={() => {
                        dispatch(closeDisplayTask())
                        dispatch(openDeleteTask())
                      }}
                    >
                      delete task
                    </button>
                  </div>
                </div>
                <RxCross2 size={24} className='title-cross' onClick={onClose} />
              </div>
            </div>
            <p className='description'>{currentTask.description}</p>
            {currentTask.subtasks.length === 0 ? (
              <p className='subtasks-title'>No subtasks</p>
            ) : (
              <p className='subtasks-title'>{`Subtasks (${
                currentTask.subtasks.filter((subtask) => subtask.isCompleted)
                  .length
              } of ${currentTask.subtasks.length})`}</p>
            )}
            <ul className='subtasks-container'>
              {currentTask.subtasks.map((subtask, index) => (
                <li key={index} className='subtasks'>
                  <input
                    type='checkbox'
                    name={`check${index}`}
                    checked={subtask.isCompleted}
                    className='checkbox'
                    onChange={() => handleToggleSubtask(index)}
                  />
                  <label
                    htmlFor={`check${index}`}
                    className={`${
                      subtask.isCompleted
                        ? 'subtask-title completed'
                        : 'subtask-title '
                    }`}
                  >
                    {subtask.title}
                  </label>
                </li>
              ))}
            </ul>
            <p className='subtasks-title'>Current Status</p>
            <div
              className='status-container'
              onClick={() => setShowStatusDropdown(!showStatusDropdown)}
            >
              <p>{currentTask.status}</p>
              <img src={chevronDown} alt='chevron-down' className='chevron' />
              {showStatusDropdown && (
                <ul className='status-dropdown'>
                  {currentColumns.map((column) => (
                    <li
                      key={column._id}
                      className='status-option'
                      onClick={() => handleStatusChange(column._id!)}
                    >
                      {column.name}
                    </li>
                  ))}
                </ul>
              )}
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}

export default TaskDisplay
