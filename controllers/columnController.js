import { nanoid } from 'nanoid'
import Column from '../models/columnModel.js'
import { StatusCodes } from 'http-status-codes'
import { NotFoundError } from '../errors/customError.js'

export const getAllColumns = async (req, res) => {
  const columns = await Column.find({ createdBy: req.user.userId })
  res.status(StatusCodes.OK).json({ columns })
}

export const createColumn = async (req, res) => {
  req.body.createdBy = req.user.userId
  const column = await Column.create(req.body)
  res.status(StatusCodes.CREATED).json({ column })
}

// export const createColumn = async (req, res) => {
//   req.body.createdBy = req.user.userId
//   // On recupere le nom et les colonnes du body
//   const { name, columns } = req.body

//   const board = await Board.create({ name, createdBy: req.user.userId })

//   // On crée un tableau de colonnes et on recupere les ids
//   const createdColumns = await Column.create(
//     columns.map((col) => ({ name: col.name, boardId: board._id }))
//   )
//   const columnIds = createdColumns.map((col) => col._id)

//   // On met a jour le board avec les ids des colonnes
//   const updatedBoard = await Board.findByIdAndUpdate(
//     board._id,
//     { columns: columnIds },
//     { new: true }
//   )

//   res.status(StatusCodes.CREATED).json({ updatedBoard })
// }

// export const getColumn = async (req, res) => {
//   const board = await Board.findById(req.params.id)

//   res.status(StatusCodes.OK).json({ board })
// }

// export const updateColumn = async (req, res) => {
//   const updatedBoard = await Board.findByIdAndUpdate(req.params.id, req.body, {
//     new: true,
//   })

//   res.status(StatusCodes.OK).json({ msg: 'board modified', updatedBoard })
// }

// export const deleteColumn = async (req, res) => {
//   const removedBoard = await Board.findByIdAndDelete(req.params.id)

//   res.status(StatusCodes.OK).json({ msg: 'board deleted', board: removedBoard })
// }
