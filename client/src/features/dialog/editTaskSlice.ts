import { createSlice } from '@reduxjs/toolkit'

type EditTaskState = {
  isOpen: boolean
}

const initialState: EditTaskState = {
  isOpen: false,
}

const editTaskSlice = createSlice({
  name: 'editTask',
  initialState,
  reducers: {
    openEditTask: (state) => {
      state.isOpen = true
    },
    closeEditTask: (state) => {
      state.isOpen = false
    },
  },
})

export const { openEditTask, closeEditTask } = editTaskSlice.actions

export default editTaskSlice.reducer
