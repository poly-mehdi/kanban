import mongoose from 'mongoose'
import { TaskSchema } from './taskModel.js'

export const ColumnSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  boardId: { type: mongoose.Schema.Types.ObjectId, ref: 'Board' },
  tasks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Task' }],
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
})

export default mongoose.model('Column', ColumnSchema)
