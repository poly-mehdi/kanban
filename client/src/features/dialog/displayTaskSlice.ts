import { createSlice } from '@reduxjs/toolkit'

type DisplayTaskState = {
  isOpen: boolean
}

const initialState: DisplayTaskState = {
  isOpen: false,
}

const displayTaskSlice = createSlice({
  name: 'displayTask',
  initialState,
  reducers: {
    openDisplayTask: (state) => {
      state.isOpen = true
    },
    closeDisplayTask: (state) => {
      state.isOpen = false
    },
  },
})

export const { openDisplayTask, closeDisplayTask } = displayTaskSlice.actions

export default displayTaskSlice.reducer
