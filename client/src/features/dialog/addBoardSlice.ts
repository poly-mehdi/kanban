import { createSlice } from '@reduxjs/toolkit'

type AddBoardState = {
  isOpen: boolean
}

const initialState: AddBoardState = {
  isOpen: false,
}

const createBoardSlice = createSlice({
  name: 'createBoard',
  initialState,
  reducers: {
    openAddBoard: (state) => {
      state.isOpen = true
    },
    closeAddBoard: (state) => {
      state.isOpen = false
    },
  },
})

export const { openAddBoard, closeAddBoard } = createBoardSlice.actions

export default createBoardSlice.reducer
